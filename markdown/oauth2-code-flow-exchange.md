# OAuth2 Code flow exchange

## Meta

### Blog title

Using OAuth2 Authorization Code flow in environment that cannot be accessed
from the outside

Open to suggestions

### Who are you writing this blog for?

Engineers who enjoy using software in a ways that it was not design to be
used but it still works. It will not be super complicated but more unorthodox.
Essentially a hack

### What is the blog about?

Making OAuth2 code flow working when you cannot post back to server that
initiated authentication. I will use example of Renew but it can be used in
other situations. It will be 3 part blog: problem + proof of concept,
target hosting platform + implementation and hosting + final result.

### Is there anything you want us to address?

Naming is the hardest thing. English grammar is the close second.

## Part I: Problem and Proof of Concept

### Intro

**CAUTION**: A lot of the things in this blog series describe how I used
software in a way that was not designed to be used. I am also not a security
specialist so I cannot guarantee that what I am describing will not put your
credentials at risk. If you decide to reproduce any of it, I take no
responsibility.

hx is ISO27001 certified which requires us to submit Request for Change (RFC)
forms whenever we make any significant or riskier change in the system, such
as releasing new version of our software. Being an engineer, Word documents are
not my preferred way of conveying any information. So I decided to automate for
creation in our Insurance Pricing platform Renew, which you can find blog post
about [here](https://www.hyperexponential.com/blog/automating-iso-change-requests).

This worked fine but it automated only part of the process - generating Word
document. We still need to write an email include all participants, add
attachment and write a message. Obviously, spending extra 5 minutes 10 times a
year is too much, so I decided to spend many hours to automate that part again.
Key blocker is to send email as myself. Renew has a feature for storing
parameters, such as API keys. This works great for system to system integration
but I don't want to give Renew model access to send email as anyone. OAuth2
Authorization Code flow (code flow for short) would be suitable and it is very
widely supported. There are a lot of resources about this OAuth2 flow but for
getting started, I really enjoyed this [article](https://www.oauth.com/oauth2-servers/server-side-apps/authorization-code/).
The problem with this is that callback needs to reach your server with the
code. However, Renew model backend does not allow running a server with
exposed port that would receive this callback. Unless you had an intermediary
exchange server of some sorts that could relay the message to whoever started
code flow. And as fundamental theorem of software engineering states:

> We can solve any problem by introducing an extra level of indirection.

### Design

Assumptions about the consumer:

- It can store credentials securely
- It can make HTTP calls
- It can send a link to the user which would be used to initiate code flow.

The simplest consumer would be a script that can read credentials from
environment variables and prints URL to Stdout.

Exchange server needs at least 2 endpoints:

- To receive authorization code, usually referred as "callback" endpoint
- To retrieve authorization code by the consumer

Fortunately, code flow already supports state parameter that would be included
in the callback, which can be used to map between those two endpoints.
Reliability and performance is not a concern for now, so long polling would be
the simplest option with the lowest latency for retrieving authorization code.
Polling on a client side would be acceptable as well but ideally, client would
be kept as simple as possible. Database is also not necessary as I could store
any transient data in memory.

![Initial design diagram](oauth2-code-flow-exchange/initial_design.png)

### Consumer

Requirements

```text
requests==2.26.0
```

Source

```python
import os
import random
import string
import urllib.parse

import requests

# Application config
CLIENT_ID = os.environ["CLIENT_ID"]
CLIENT_SECRET = os.environ["CLIENT_SECRET"]
TOKEN_URL = os.environ["TOKEN_URL"]
AUTHORIZATION_URL = os.environ["AUTHORIZATION_URL"]
PROXY_URL = os.environ["PROXY_URL"]


def main() -> None:
    # Passing in state will allow matching long polling request with callback
    state = "".join(
        random.choices(string.ascii_uppercase + string.digits, k=32)
    )
    # Redirect/Callback URL to the proxy server
    redirect_url = f"{PROXY_URL}/oauth2/callback"
    # Standard parameters for code flow. You may need to add scope or audience,
    # depending on your identity provider
    params = {
        "client_id": CLIENT_ID,
        "redirect_uri": redirect_url,
        "response_type": "code",
        "state": state,
    }

    print("Please click on a link to authorize")
    print(f"{AUTHORIZATION_URL}?{urllib.parse.urlencode(params)}")

    # Start waiting for the callback request, pass in state to do matching
    response = requests.post(
        f"{PROXY_URL}/wait-for-callback", params={"state": state}
    )
    response.raise_for_status()
    # Get authorization code from the response
    authorization_code = response.json()["code"]

    # Use authorization code to get access token
    response = requests.post(
        TOKEN_URL,
        # More parameters may be needed, depending on your identity provider
        data={
            "grant_type": "authorization_code",
            "redirect_uri": redirect_url,
            "code": authorization_code,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
        },
    )
    response.raise_for_status()
    # Get access token. You may want to get other info depending on your
    # identity provider and situation
    access_token = response.json()["access_token"]
    print("Your access token")
    print(access_token)


if __name__ == "__main__":
    main()
```

### Exchange server

Requirements

```text
sanic==21.9.1
```

Source

```python
import asyncio
import json
from typing import Dict, Optional, TypeVar, Generic

from sanic import Sanic, response
from sanic.exceptions import SanicException
from sanic.request import Request
from sanic.response import HTTPResponse

app = Sanic("OAuth2 Authorization code flow exchange")

T = TypeVar("T")


class WaitableValue(Generic[T]):
    """
    Helper class to associate a value with asyncio.Event. This allows
    waiting for the value to be available
    """

    def __init__(self) -> None:
        self._event: asyncio.Event = asyncio.Event()
        self._value: Optional[T] = None

    async def wait(self, timeout: float) -> bool:
        """
        Wait for the value to be ready. Return if the value is set.
        """
        try:
            await asyncio.wait_for(self._event.wait(), timeout)
        except asyncio.TimeoutError:
            pass
        return self._event.is_set()

    def get(self) -> Optional[T]:
        return self._value

    def set(self, value: T) -> None:
        self._value = value
        self._event.set()


# In memory storage for keeping mappings
storage: Dict[str, WaitableValue[str]] = {}


def get_or_create_authorization_code(state: str) -> WaitableValue[str]:
    """
    Retrieve authorization code from storage. If it does not exist, create it
    and add to storage before returning.
    """
    try:
        return storage[state]
    except KeyError:
        value: WaitableValue[str] = WaitableValue()
        storage[state] = value
        return value


@app.post("/wait-for-callback")
async def wait_for_callback(request: Request) -> HTTPResponse:
    """
    Wait for the authorization code to be available. Use state parameter to map 
    between this long polling request and callback from identity provider
    """
    # Get and validate state
    state: Optional[str] = request.args.get("state")
    if not state:
        raise SanicException("Missing state parameter", 400)
    authorization_code = get_or_create_authorization_code(state)

    # Start streaming response
    streaming_response: HTTPResponse = await request.respond(
        content_type="application/json"
    )
    # Send space character every 4 seconds. This is to make sure that
    # connection is kept open while we are waiting for callback to finish.
    # Various proxies and loadbalancers have tendency to kill the connection
    # if no data is exchanged for some time. Sending space will not affect the
    # response because we are using JSON.
    while not await authorization_code.wait(4):
        await streaming_response.send(b" ")
    # Respond with authorization code
    await streaming_response.send(
        json.dumps({"code": authorization_code.get()}).encode()
    )

    # Finish streaming
    await streaming_response.send(end_stream=True)
    return streaming_response


@app.get("/oauth2/callback")
async def callback(request: Request) -> HTTPResponse:
    """
    Store authorization code for particular state
    """
    # Get and validate state
    state: Optional[str] = request.args.get("state")
    if not state:
        raise SanicException("Missing state parameter", 400)
    authorization_code = get_or_create_authorization_code(state)

    # Get and validate state
    if authorization_code.get() is not None:
        raise SanicException(
            "Authorization code already received for this state",
            400,
        )

    # Get and validate authorization code
    code: Optional[str] = request.args.get("code")
    if not code:
        raise SanicException("Missing authorization code parameter", 400)

    # Store authorization code
    authorization_code.set(code)

    # Write a nice message to the user saying that authentication was successful
    return response.html(
        """
<html>
<head>
</head>
<body>
You have logged in. You can close this page and go back to your application
</body>
</html>
"""
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
```
