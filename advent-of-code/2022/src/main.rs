use clap::Parser;
use std::fmt::{Display, Formatter};
use std::fs;

mod day1;
mod day2;
mod day3;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    day: u8,
}

fn read_input(day: u8) -> String {
    fs::read_to_string(format!("input/day{day}.txt")).expect("file exists")
}

fn main() {
    let args = Args::parse();
    match args.day {
        1 => print!("{}", day1::solve(read_input(args.day).as_str())),
        2 => print!("{}", day2::solve(read_input(args.day).as_str())),
        3 => print!("{}", day3::solve(read_input(args.day).as_str())),
        _ => print!(
            "{}",
            Answer {
                part1: "unknown day",
                part2: "unknown day"
            }
        ),
    }
}

#[derive(PartialEq, Debug)]
pub struct Answer<P1: Display, P2: Display> {
    part1: P1,
    part2: P2,
}

impl<P1: Display, P2: Display> Display for Answer<P1, P2> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Part 1: {}", self.part1)?;
        writeln!(f, "Part 2: {}", self.part2)?;
        Ok(())
    }
}
