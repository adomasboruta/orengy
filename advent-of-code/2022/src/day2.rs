use crate::Answer;

#[derive(Clone, Copy)]
enum RPS {
    Rock,
    Paper,
    Scissors,
}

struct Round {
    opponent: RPS,
    me: RPS,
}

impl Round {
    fn score(&self) -> i32 {
        let match_score = match (self.opponent, self.me) {
            (RPS::Rock, RPS::Paper) | (RPS::Paper, RPS::Scissors) | (RPS::Scissors, RPS::Rock) => 6,
            (RPS::Paper, RPS::Rock) | (RPS::Scissors, RPS::Paper) | (RPS::Rock, RPS::Scissors) => 0,
            _ => 3,
        };
        let sign_score = match self.me {
            RPS::Rock => 1,
            RPS::Paper => 2,
            RPS::Scissors => 3,
        };
        return match_score + sign_score;
    }
}

pub fn solve(input: &str) -> Answer<i32, i32> {
    let rounds: Vec<(Round, Round)> = input
        .lines()
        .map(|line| {
            let opponent = match line.chars().nth(0) {
                Some('A') => RPS::Rock,
                Some('B') => RPS::Paper,
                Some('C') => RPS::Scissors,
                _ => panic!("invalid opponent sign"),
            };
            let me = match line.chars().nth(2) {
                Some('X') => RPS::Rock,
                Some('Y') => RPS::Paper,
                Some('Z') => RPS::Scissors,
                _ => panic!("invalid me sign"),
            };
            let me_alternative = match line.chars().nth(2) {
                // Need to lose
                Some('X') => match opponent {
                    RPS::Rock => RPS::Scissors,
                    RPS::Paper => RPS::Rock,
                    RPS::Scissors => RPS::Paper,
                },
                // Need to draw
                Some('Y') => opponent,
                // Need to win
                Some('Z') => match opponent {
                    RPS::Rock => RPS::Paper,
                    RPS::Paper => RPS::Scissors,
                    RPS::Scissors => RPS::Rock,
                },
                _ => panic!("invalid me_alternative sign"),
            };
            (
                Round { opponent, me },
                Round {
                    opponent,
                    me: me_alternative,
                },
            )
        })
        .collect();

    let part1 = rounds.iter().map(|(round, _)| round.score()).sum();
    let part2 = rounds.iter().map(|(_, round)| round.score()).sum();

    return Answer { part1, part2 };
}

#[cfg(test)]
mod tests {
    #[test]
    fn solve() {
        assert_eq!(
            super::solve(
                "A Y
B X
C Z
"
            ),
            crate::Answer {
                part1: 15,
                part2: 12,
            }
        );
    }
}
