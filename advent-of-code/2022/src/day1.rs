use crate::Answer;

pub fn solve(input: &str) -> Answer<i32, i32> {
    let mut items: Vec<i32> = vec![0];

    for line in input.lines() {
        if line == "" {
            items.push(0);
            continue;
        }

        let num = line.parse::<i32>().expect("invalid number");
        let last_item = items
            .last_mut()
            .expect("at least one group always available in the list");
        *last_item += num;
    }

    items.sort();

    return Answer {
        part1: items.iter().rev().take(1).sum(),
        part2: items.iter().rev().take(3).sum(),
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn solve() {
        assert_eq!(
            super::solve(
                "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000"
            ),
            crate::Answer {
                part1: 24000,
                part2: 45000,
            }
        );
    }
}
