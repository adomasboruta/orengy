use std::collections::HashSet;

use itertools::Itertools;

use crate::Answer;

pub fn solve(input: &str) -> Answer<usize, usize> {
    let values = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    let mut part1 = 0;
    for line in input.lines() {
        let first_half: HashSet<char> = line.chars().take(line.chars().count() / 2).collect();
        let second_half: HashSet<char> = line.chars().skip(line.chars().count() / 2).collect();
        let shared = first_half
            .intersection(&second_half)
            .next()
            .expect("at least one item shared between halfs");

        part1 += values
            .chars()
            .position(|c| c == *shared)
            .expect("part of alphabet")
            + 1;
    }

    let mut part2 = 0;

    for (first, second, third) in input.lines().tuples() {
        let first_set = first.chars().collect::<HashSet<char>>();
        let second_set = second.chars().collect::<HashSet<char>>();
        let third_set = third.chars().collect::<HashSet<char>>();

        let first_second_intersection = first_set
            .intersection(&second_set)
            .cloned()
            .collect::<HashSet<char>>();

        let identity = first_second_intersection
            .intersection(&third_set)
            .next()
            .expect("at least one item for identification");

        part2 += values
            .chars()
            .position(|c| c == *identity)
            .expect("part of alphabet")
            + 1;
    }

    return Answer { part1, part2 };
}

#[cfg(test)]
mod tests {
    #[test]
    fn solve() {
        assert_eq!(
            super::solve(
                "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw"
            ),
            crate::Answer {
                part1: 157,
                part2: 70,
            }
        );
    }
}
