CREATE TEMP TABLE lines AS
SELECT line,
       SUBSTR(line, 1, INSTR(line, '-') - 2)                                          AS start,
       SUBSTR(line, INSTR(line, '>') + 2)                                             AS end,

       CAST(SUBSTR(SUBSTR(line, 1, INSTR(line, '-') - 2), 1,
                   INSTR(SUBSTR(line, 1, INSTR(line, '-') - 2), ',') - 1) AS INTEGER) AS x_start,
       CAST(SUBSTR(SUBSTR(line, 1, INSTR(line, '-') - 2),
                   INSTR(SUBSTR(line, 1, INSTR(line, '-') - 2), ',') + 1) AS INTEGER) AS y_start,

       CAST(SUBSTR(SUBSTR(line, INSTR(line, '>') + 2), 1,
                   INSTR(SUBSTR(line, INSTR(line, '>') + 2), ',') - 1) AS INTEGER)    AS x_end,
       CAST(SUBSTR(SUBSTR(line, INSTR(line, '>') + 2),
                   INSTR(SUBSTR(line, INSTR(line, '>') + 2), ',') + 1) AS INTEGER)    AS y_end
FROM day5_input;

SELECT COUNT(*) AS part1
FROM (
         WITH points(x_start, y_start, x_end, y_end) AS (
             SELECT x_start, y_start, x_end, y_end
             FROM lines
             WHERE x_start = x_end
                OR y_start = y_end
             UNION ALL
             SELECT CASE
                        WHEN x_start < x_end THEN x_start + 1
                        WHEN x_start > x_end THEN x_start - 1
                        ELSE x_start
                        END AS x_start,
                    CASE
                        WHEN y_start < y_end THEN y_start + 1
                        WHEN y_start > y_end THEN y_start - 1
                        ELSE y_start
                        END AS y_start,
                    x_end,
                    y_end
             FROM points
             WHERE x_start != x_end
                OR y_start != y_end
         )
         SELECT x_start AS x, y_start AS y
         FROM points
         GROUP BY x_start, y_start
         HAVING COUNT(*) > 1
     );

SELECT COUNT(*) AS part2
FROM (
         WITH points(x_start, y_start, x_end, y_end) AS (
             SELECT x_start, y_start, x_end, y_end
             FROM lines
             UNION ALL
             SELECT CASE
                        WHEN x_start < x_end THEN x_start + 1
                        WHEN x_start > x_end THEN x_start - 1
                        ELSE x_start
                        END AS x_start,
                    CASE
                        WHEN y_start < y_end THEN y_start + 1
                        WHEN y_start > y_end THEN y_start - 1
                        ELSE y_start
                        END AS y_start,
                    x_end,
                    y_end
             FROM points
             WHERE x_start != x_end
                OR y_start != y_end
         )
         SELECT x_start AS x, y_start AS y
         FROM points
         GROUP BY x_start, y_start
         HAVING COUNT(*) > 1
     );