CREATE TEMP TABLE input_digits AS
WITH display(
             display_id,
             encoded_digit,
             input
    ) AS (
    SELECT rowid                                                                       AS display_id,
           SUBSTR(line, 1, INSTR(line, ' ') - 1)                                       AS encoded_digit,
           SUBSTR(line, INSTR(line, ' ') + 1, INSTR(line, '|') - INSTR(line, ' ') - 1) AS input
    FROM day8_input
    UNION ALL
    SELECT display_id,
           SUBSTR(input, 1, INSTR(input, ' ') - 1) AS encoded_digit,
           SUBSTR(input, INSTR(input, ' ') + 1)    AS input
    FROM display
    WHERE input != ''
)
SELECT display_id,
       encoded_digit,
       iif(INSTR(encoded_digit, 'a') != 0, 1 << 0, 0) |
       iif(INSTR(encoded_digit, 'b') != 0, 1 << 1, 0) |
       iif(INSTR(encoded_digit, 'c') != 0, 1 << 2, 0) |
       iif(INSTR(encoded_digit, 'd') != 0, 1 << 3, 0) |
       iif(INSTR(encoded_digit, 'e') != 0, 1 << 4, 0) |
       iif(INSTR(encoded_digit, 'f') != 0, 1 << 5, 0) |
       iif(INSTR(encoded_digit, 'g') != 0, 1 << 6, 0) AS decoded_digit,
       CASE
           WHEN LENGTH(encoded_digit) = 2 THEN 1
           WHEN LENGTH(encoded_digit) = 3 THEN 7
           WHEN LENGTH(encoded_digit) = 4 THEN 4
           WHEN LENGTH(encoded_digit) = 7 THEN 8
           ELSE -1
           END                                        AS value
FROM display
ORDER BY display_id, LENGTH(encoded_digit);

UPDATE input_digits
SET value = 3
WHERE LENGTH(input_digits.encoded_digit) = 5
  AND input_digits.value = -1
  AND (
    SELECT TRUE
    FROM input_digits AS digit_1
    WHERE digit_1.value = 1
      AND digit_1.display_id = input_digits.display_id
      AND input_digits.decoded_digit & digit_1.decoded_digit = digit_1.decoded_digit
);

UPDATE input_digits
SET value = 6
WHERE LENGTH(input_digits.encoded_digit) = 6
  AND input_digits.value = -1
  AND (
    SELECT TRUE
    FROM input_digits AS digit_1
    WHERE digit_1.value = 1
      AND digit_1.display_id = input_digits.display_id
      AND input_digits.decoded_digit & digit_1.decoded_digit != digit_1.decoded_digit
);

UPDATE input_digits
SET value = 9
WHERE LENGTH(input_digits.encoded_digit) = 6
  AND input_digits.value = -1
  AND (
    SELECT TRUE
    FROM input_digits AS digit_7
             INNER JOIN input_digits AS digit_4 ON digit_7.display_id = digit_4.display_id AND digit_4.value = 4
             INNER JOIN input_digits AS digit_3 ON digit_7.display_id = digit_3.display_id AND digit_3.value = 3
    WHERE digit_7.value = 7
      AND digit_7.display_id = input_digits.display_id
      AND digit_7.decoded_digit | digit_4.decoded_digit | digit_3.decoded_digit = input_digits.decoded_digit
);

UPDATE input_digits
SET value = 5
WHERE LENGTH(input_digits.encoded_digit) = 5
  AND input_digits.value = -1
  AND (
    SELECT TRUE
    FROM input_digits AS digit_9
    WHERE digit_9.value = 9
      AND digit_9.display_id = input_digits.display_id
      AND input_digits.decoded_digit | digit_9.decoded_digit = digit_9.decoded_digit
);

UPDATE input_digits
SET value = 0
WHERE LENGTH(input_digits.encoded_digit) = 6
  AND input_digits.value = -1
  AND (
    SELECT TRUE
    FROM input_digits AS digit_7
    WHERE digit_7.value = 7
      AND digit_7.display_id = input_digits.display_id
      AND input_digits.decoded_digit & digit_7.decoded_digit = digit_7.decoded_digit
);

UPDATE input_digits
SET value = 2
WHERE input_digits.value = -1;

CREATE TEMP TABLE display_output AS
WITH display(
             n,
             display_id,
             encoded_digit,
             input
    ) AS (
    SELECT 0                                           AS n,
           display_id,
           SUBSTR(input, 1, INSTR(input, ' '))         AS encoded_digit,
           SUBSTR(input, INSTR(input, ' ') + 1) || ' ' AS input
    FROM (SELECT rowid                              AS display_id,
                 SUBSTR(line, INSTR(line, '|') + 2) AS input
          FROM day8_input) AS display_input
    UNION ALL
    SELECT n + 1                                   AS n,
           display_id,
           SUBSTR(input, 1, INSTR(input, ' ') - 1) AS encoded_digit,
           SUBSTR(input, INSTR(input, ' ') + 1)    AS input
    FROM display
    WHERE input != ''
)
SELECT display.n,
       display.display_id,
       display.encoded_digit,
       input_digits.value
FROM display
         INNER JOIN input_digits ON display.display_id = input_digits.display_id AND
                                    iif(INSTR(display.encoded_digit, 'a') != 0, 1 << 0, 0) |
                                    iif(INSTR(display.encoded_digit, 'b') != 0, 1 << 1, 0) |
                                    iif(INSTR(display.encoded_digit, 'c') != 0, 1 << 2, 0) |
                                    iif(INSTR(display.encoded_digit, 'd') != 0, 1 << 3, 0) |
                                    iif(INSTR(display.encoded_digit, 'e') != 0, 1 << 4, 0) |
                                    iif(INSTR(display.encoded_digit, 'f') != 0, 1 << 5, 0) |
                                    iif(INSTR(display.encoded_digit, 'g') != 0, 1 << 6, 0) = input_digits.decoded_digit
ORDER BY display.display_id, display.n;

SELECT COUNT(*) AS part1
FROM display_output
WHERE value IN (1, 4, 7, 8);

SELECT SUM(CASE n
               WHEN 0 THEN 1000 * value
               WHEN 1 THEN 100 * value
               WHEN 2 THEN 10 * value
               ELSE value
    END) AS part2
FROM display_output;
