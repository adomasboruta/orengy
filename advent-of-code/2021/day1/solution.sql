CREATE
TEMP VIEW day1 AS
SELECT rowid, CAST(line AS INTEGER) AS value
FROM day1_input;

SELECT SUM(CASE WHEN previous.value < current.value THEN 1 ELSE 0 END) as part1
FROM day1 AS current
INNER JOIN day1 as previous
ON current.rowid = previous.rowid + 1;

SELECT SUM(CASE WHEN previous.value < current.value THEN 1 ELSE 0 END) as part2
FROM day1 AS current
INNER JOIN day1 as previous
ON current.rowid = previous.rowid + 3;
