#!/usr/bin/env python3

import glob
import os
import sqlite3


def main():
    try:
        os.unlink("sqlite.db")
    except FileNotFoundError:
        pass
    con = sqlite3.connect("sqlite.db")
    cur = con.cursor()

    for folder in glob.glob("day*"):
        cur.execute(f"CREATE TABLE {folder}_input (line TEXT)")
        with open(os.path.join(folder, "input")) as f:
            cur.executemany(f"INSERT INTO {folder}_input VALUES (?)", [(v,) for v in f.read().splitlines()])
    con.commit()
    con.close()


if __name__ == '__main__':
    main()
