# Advent of code 2021

Challenge is to solve everything using SQL.

Requirements:
- `python3`
- `docker`

## Setup

Every day will have 1 folder in the form of `day{i}`. Every folder should 
contain `input` file with that days challenge and `solution.sql` with the
solution. 

`generate_db.py` script takes in all inputs and creates database of the form

```sql
CREATE TABLE day{i}_input (line TEXT)
```

## Running

Running solve script will execute solution from particular day

```shell
./solve day{i}
```
