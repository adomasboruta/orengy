CREATE TEMP TABLE cave_edge AS
WITH bidirectional_edge AS (
    SELECT SUBSTR(line, 1, INSTR(line, '-') - 1) AS end1, SUBSTR(line, INSTR(line, '-') + 1) AS end2
    FROM day12_input
),
     unidirectional_edge AS (
         SELECT end1 AS from_cave, end2 AS to_cave
         FROM bidirectional_edge
         UNION ALL
         SELECT end2 AS from_cave, end1 AS to_cave
         FROM bidirectional_edge
     )
SELECT from_cave, to_cave, LOWER(to_cave) = to_cave AS is_to_cave_small
FROM unidirectional_edge;

WITH path(current_path, previous_cave) AS (
    SELECT 'start,' AS current_path, 'start' AS previous_cave
    UNION ALL
    SELECT path.current_path || cave_edge.to_cave || ',' AS current_path, cave_edge.to_cave AS previous_cave
    FROM path
             INNER JOIN cave_edge ON path.previous_cave = cave_edge.from_cave
    WHERE path.previous_cave != 'end'
      AND (NOT cave_edge.is_to_cave_small OR path.current_path NOT LIKE '%' || cave_edge.to_cave || ',%')
)
SELECT COUNT(*) AS part1
FROM path
WHERE previous_cave = 'end';


WITH path(current_path, previous_cave, visited_small_cave_twice) AS (
    SELECT 'start,' AS current_path,
           'start'  AS previous_cave,
           FALSE    AS visited_small_cave_twice
    UNION ALL
    SELECT path.current_path || cave_edge.to_cave || ',' AS current_path,
           cave_edge.to_cave                             AS previous_cave,
           visited_small_cave_twice
               OR (
                   cave_edge.is_to_cave_small
                   AND path.current_path LIKE '%' || cave_edge.to_cave || ',%'
               )                                         AS visited_small_cave_twice
    FROM path
             INNER JOIN cave_edge ON path.previous_cave = cave_edge.from_cave
    WHERE path.previous_cave != 'end'
      AND cave_edge.to_cave != 'start'
      AND (
            NOT cave_edge.is_to_cave_small
            OR NOT path.visited_small_cave_twice
            OR path.current_path NOT LIKE '%' || cave_edge.to_cave || ',%')
)
SELECT COUNT(*) AS part2
FROM path
WHERE previous_cave = 'end';
