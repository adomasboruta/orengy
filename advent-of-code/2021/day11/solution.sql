WITH octopus_map(step, input) AS (
    SELECT 0 AS step, GROUP_CONCAT(line, '') AS input
    FROM day11_input
    UNION ALL
    SELECT iif(INSTR(input, '.') = 0 AND INSTR(input, 'X') = 0, step + 1, step) AS step,
           CASE
               WHEN INSTR(input, 'X') > 0 THEN
                       SUBSTR(input, 1, INSTR(input, 'X') - 1) ||
                       REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                                                                               REPLACE(REPLACE(REPLACE(
                                                                                                       REPLACE(SUBSTR(input, INSTR(input, 'X') - 1, 3), 'X', '.'),
                                                                                                       '9', 'X'),
                                                                                               '8', '9'), '7', '8'),
                                                                               '6', '7'), '5', '6'), '4', '5'), '3',
                                                       '4'),
                                               '2', '3'), '1', '2'), '0', '1')
                       ||
                       SUBSTR(input, INSTR(input, 'X') + 1)


               WHEN INSTR(input, '.') > 0 THEN REPLACE(input, '.', '0')
               ELSE
                   REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                                                                           REPLACE(REPLACE(REPLACE(input, '9', 'X'), '8', '9'), '7', '8'),
                                                                           '6', '7'), '5', '6'), '4', '5'), '3', '4'),
                                           '2', '3'), '1', '2'), '0', '1')
               END                                                              AS input
    FROM octopus_map
    WHERE step < 10
)
SELECT step,
       SUBSTR(input, 1, 10) || '
' || SUBSTR(input, 11, 10) || '
' || SUBSTR(input, 21, 10) || '
' || SUBSTR(input, 31, 10) || '
' || SUBSTR(input, 41, 10) || '
' || SUBSTR(input, 51, 10) || '
' || SUBSTR(input, 61, 10) || '
' || SUBSTR(input, 71, 10) || '
' || SUBSTR(input, 81, 10) || '
' || SUBSTR(input, 91, 10)
FROM octopus_map



