# # Sample
# data = [
#     [5, 4, 8, 3, 1, 4, 3, 2, 2, 3],
#     [2, 7, 4, 5, 8, 5, 4, 7, 1, 1],
#     [5, 2, 6, 4, 5, 5, 6, 1, 7, 3],
#     [6, 1, 4, 1, 3, 3, 6, 1, 4, 6],
#     [6, 3, 5, 7, 3, 8, 5, 4, 7, 8],
#     [4, 1, 6, 7, 5, 2, 4, 6, 4, 5],
#     [2, 1, 7, 6, 8, 4, 1, 7, 2, 1],
#     [6, 8, 8, 2, 8, 8, 1, 1, 3, 4],
#     [4, 8, 4, 6, 8, 4, 8, 5, 5, 4],
#     [5, 2, 8, 3, 7, 5, 1, 5, 2, 6],
# ]

# Input

data = [
    [6, 7, 8, 8, 3, 8, 3, 4, 3, 6],
    [5, 5, 2, 6, 8, 2, 7, 4, 4, 1],
    [4, 5, 8, 2, 4, 3, 5, 8, 6, 6],
    [5, 1, 5, 2, 5, 4, 7, 2, 7, 3],
    [3, 7, 4, 6, 4, 3, 3, 6, 2, 1],
    [2, 4, 6, 5, 1, 4, 5, 3, 6, 5],
    [6, 3, 2, 4, 8, 8, 7, 1, 2, 8],
    [8, 5, 3, 7, 5, 5, 8, 7, 4, 5],
    [4, 7, 1, 8, 4, 2, 7, 5, 6, 2],
    [2, 2, 8, 3, 3, 2, 4, 7, 4, 6],
]

if __name__ == '__main__':
    w, h = 10, 10
    total_flashes = 0
    i = 1
    while True:
        about_to_flash = []
        for row_index, row in enumerate(data):
            for column_index, value in enumerate(row):
                if value == 9:
                    about_to_flash.append((row_index, column_index))
                data[row_index][column_index] = value + 1
        while about_to_flash:
            row_index, column_index = about_to_flash.pop()
            for row_adj, column_adj in (
                    (-1, -1),
                    (-1, 0),
                    (-1, 1),
                    (0, -1),
                    (0, 0),
                    (0, 1),
                    (1, -1),
                    (1, 0),
                    (1, 1),
            ):
                if 0 <= row_index + row_adj < w and 0 <= column_index + column_adj < h:
                    value = data[row_index + row_adj][column_index + column_adj]
                    if value == 9:
                        about_to_flash.append((row_index + row_adj, column_index + column_adj))
                    data[row_index + row_adj][column_index + column_adj] = value + 1
        current_flashes = 0
        for row_index, row in enumerate(data):
            for column_index, value in enumerate(row):
                if value > 9:
                    total_flashes += 1
                    current_flashes += 1
                    data[row_index][column_index] = 0
        if i == 100:
            print("part1:", total_flashes)
        if current_flashes == 100:
            print("part2:", i)
            break
        i += 1


