CREATE TEMP TABLE crab AS
WITH crab(i, number, input) AS (
    SELECT 0                                                  AS i,
           CAST(SUBSTR(line, 0, INSTR(line, ',')) AS INTEGER) AS number,
           SUBSTR(line, INSTR(line, ',') + 1) || ','          AS input
    FROM day7_input
    WHERE rowid = 1
    UNION ALL
    SELECT i + 1                                                AS i,
           CAST(SUBSTR(input, 0, INSTR(input, ',')) AS INTEGER) AS number,
           SUBSTR(input, INSTR(input, ',') + 1)                 AS input
    FROM crab
    WHERE input != ''
)
SELECT i, number
FROM crab;

SELECT MIN(number) AS part1
FROM (
         SELECT SUM(ABS(crab.number - optimal.number)) AS number
         FROM crab
                  CROSS JOIN (
             SELECT DISTINCT number
             FROM crab
         ) AS optimal
         GROUP BY optimal.number
     );


SELECT MIN(number) AS part2
FROM (
         SELECT SUM((ABS(crab.number - optimal.number) +
                     (crab.number - optimal.number) * (crab.number - optimal.number)) / 2) AS number
         FROM crab
                  CROSS JOIN (
             WITH range(n, max) AS (
                 SELECT MIN(number) AS n, MAX(number) AS max
                 FROM crab
                 UNION ALL
                 SELECT n + 1 AS n, max
                 FROM range
                 WHERE n < max
             )
             SELECT n AS number
             FROM range
         ) AS optimal
         GROUP BY optimal.number
     );
