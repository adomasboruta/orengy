CREATE TEMP TABLE lava_map AS
WITH lava_map(x, y, value, input) AS (
    SELECT 1                                   AS x,
           rowid                               AS y,
           CAST(SUBSTR(line, 1, 1) AS INTEGER) AS value,
           SUBSTR(line, 2)                     AS input
    FROM day9_input
    UNION ALL
    SELECT x + 1                                AS x,
           y,
           CAST(SUBSTR(input, 1, 1) AS INTEGER) AS value,
           SUBSTR(input, 2)                     AS input
    FROM lava_map
    WHERE input != ''
)
SELECT x, y, value
FROM lava_map;

CREATE INDEX lava_map_idx ON lava_map (x, y);

CREATE TEMP TABLE low_point AS
SELECT lava_map.*
FROM lava_map
         LEFT JOIN lava_map AS left_entry ON lava_map.x = left_entry.x + 1 AND lava_map.y = left_entry.y
         LEFT JOIN lava_map AS right_entry ON lava_map.x = right_entry.x - 1 AND lava_map.y = right_entry.y
         LEFT JOIN lava_map AS top_entry ON lava_map.x = top_entry.x AND lava_map.y = top_entry.y + 1
         LEFT JOIN lava_map AS bottom_entry ON lava_map.x = bottom_entry.x AND lava_map.y = bottom_entry.y - 1
WHERE (left_entry.value IS NULL OR lava_map.value < left_entry.value)
  AND (right_entry.value IS NULL OR lava_map.value < right_entry.value)
  AND (top_entry.value IS NULL OR lava_map.value < top_entry.value)
  AND (bottom_entry.value IS NULL OR lava_map.value < bottom_entry.value);

SELECT SUM(low_point.value + 1) AS part1
FROM low_point;

CREATE TEMP TABLE adjacent
(
    x_adjustment INTEGER,
    y_adjustment INTEGER
);
INSERT INTO adjacent
VALUES (0, 1),
       (0, -1),
       (1, 0),
       (-1, 0);


CREATE TEMP TABLE part2 AS
SELECT COUNT(*) AS size, basin_id
FROM (
         WITH basin(basin_id, x, y, value) AS (
             SELECT rowid AS basin_id, x, y, value
             FROM low_point
             UNION ALL
             SELECT basin_id,
                    lava_map.x,
                    lava_map.y,
                    lava_map.value
             FROM basin
                      CROSS JOIN adjacent
                      INNER JOIN lava_map
                                 ON lava_map.x = basin.x + adjacent.x_adjustment AND
                                    lava_map.y = basin.y + adjacent.y_adjustment
             WHERE lava_map.value < 9
               AND basin.value < lava_map.value
         )
         SELECT DISTINCT *
         FROM basin
     )
GROUP BY basin_id
ORDER BY COUNT(*) DESC
LIMIT 3;

SELECT (SELECT size FROM part2 WHERE rowid = 1) *
       (SELECT size FROM part2 WHERE rowid = 2) *
       (SELECT size FROM part2 WHERE rowid = 3) AS part2
