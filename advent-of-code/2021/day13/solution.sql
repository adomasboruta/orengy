WITH point AS (
    SELECT CAST(SUBSTR(line, 1, INSTR(line, ',') - 1) AS INTEGER) AS x,
           CAST(SUBSTR(line, INSTR(line, ',') + 1) AS INTEGER)    AS y
    FROM day13_input
    WHERE line LIKE '%,%'
),
     fold AS (
         SELECT ROW_NUMBER() OVER ()                                AS fold_id,
                SUBSTR(line, 12, 1)                                 AS along_axis,
                CAST(SUBSTR(line, INSTR(line, '=') + 1) AS INTEGER) AS value
         FROM day13_input
         WHERE line LIKE 'fold along %=%'
     ),
     applied_fold(fold_id, x, y) AS (
         SELECT 0, x, y
         FROM point
         UNION
         SELECT applied_fold.fold_id + 1 AS fold_id,
                iif(fold.along_axis = 'x', fold.value - ABS(fold.value - applied_fold.x), applied_fold.x),
                iif(fold.along_axis = 'y', fold.value - ABS(fold.value - applied_fold.y), applied_fold.y)
         FROM applied_fold
                  INNER JOIN fold ON fold.fold_id = applied_fold.fold_id + 1
     ),
     part1 AS (
         SELECT COUNT(*) AS part1
         FROM applied_fold
         WHERE fold_id = 1
     ),
     final_point AS (
         SELECT *
         FROM applied_fold
         WHERE fold_id = (SELECT MAX(fold_id) FROM fold)
     ),
     x_range(n) AS (
         SELECT (SELECT MAX(x) FROM final_point)
         UNION ALL
         SELECT n - 1
         FROM x_range
         WHERE n > 0
     ),
     y_range(n) AS (
         SELECT (SELECT MAX(y) FROM final_point)
         UNION ALL
         SELECT n - 1
         FROM y_range
         WHERE n > 0
     ),
     range AS (
         SELECT x_range.n AS x, y_range.n AS y
         FROM x_range
                  CROSS JOIN y_range
         ORDER BY y, x
     ),
     part2 AS (
         SELECT GROUP_CONCAT(iif(final_point.x IS NULL, ' ', '#'), '') AS part2
         FROM range
                  LEFT JOIN final_point ON final_point.x = range.x AND final_point.y = range.y
         GROUP BY range.y
     )
SELECT part1.part1, part2.part2
FROM part1
         CROSS JOIN part2
