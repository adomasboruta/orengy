CREATE TEMP VIEW parsed_line AS
WITH parser(line_id, current, stack, error) AS (
    SELECT rowid AS line_id, line AS current, '' AS stack, '' AS error
    FROM day10_input
    UNION ALL
    SELECT line_id,
           SUBSTR(current, 2) AS current,
           CASE
               WHEN SUBSTR(current, 1, 1) = '(' THEN ')' || stack
               WHEN SUBSTR(current, 1, 1) = '[' THEN ']' || stack
               WHEN SUBSTR(current, 1, 1) = '{' THEN '}' || stack
               WHEN SUBSTR(current, 1, 1) = '<' THEN '>' || stack
               WHEN SUBSTR(current, 1, 1) = ')' AND SUBSTR(stack, 1, 1) = ')' THEN SUBSTR(stack, 2)
               WHEN SUBSTR(current, 1, 1) = ']' AND SUBSTR(stack, 1, 1) = ']' THEN SUBSTR(stack, 2)
               WHEN SUBSTR(current, 1, 1) = '}' AND SUBSTR(stack, 1, 1) = '}' THEN SUBSTR(stack, 2)
               WHEN SUBSTR(current, 1, 1) = '>' AND SUBSTR(stack, 1, 1) = '>' THEN SUBSTR(stack, 2)
               ELSE stack
               END            AS stack,
           CASE
               WHEN SUBSTR(current, 1, 1) = ')' AND SUBSTR(stack, 1, 1) != ')' OR
                    SUBSTR(current, 1, 1) = ']' AND SUBSTR(stack, 1, 1) != ']' OR
                    SUBSTR(current, 1, 1) = '}' AND SUBSTR(stack, 1, 1) != '}' OR
                    SUBSTR(current, 1, 1) = '>' AND SUBSTR(stack, 1, 1) != '>' THEN SUBSTR(current, 1, 1)
               ELSE ''
               END            AS error
    FROM parser
    WHERE error = ''
      AND current != ''
)
SELECT line_id, stack, error
FROM parser
WHERE error != ''
   OR current = '';

SELECT SUM(
               CASE error
                   WHEN ')' THEN 3
                   WHEN ']' THEN 57
                   WHEN '}' THEN 1197
                   WHEN '>' THEN 25137
                   ELSE 0
                   END
           ) AS part1
FROM parsed_line;

WITH auto_complete_score(line_id, stack, score) AS (
    SELECT line_id, stack, 0 AS score
    FROM parsed_line
    WHERE error = ''
    UNION ALL
    SELECT line_id,
           SUBSTR(stack, 2) AS stack,
           score * 5 + CASE SUBSTR(stack, 1, 1)
                           WHEN ')' THEN 1
                           WHEN ']' THEN 2
                           WHEN '}' THEN 3
                           WHEN '>' THEN 4
               END          AS score
    FROM auto_complete_score
    WHERE stack != ''
),
     final_score AS (
         SELECT *
         FROM auto_complete_score
         WHERE stack = ''
     )
SELECT score AS part2
FROM final_score
ORDER BY score
LIMIT 1 OFFSET (SELECT COUNT(*) FROM final_score) / 2
