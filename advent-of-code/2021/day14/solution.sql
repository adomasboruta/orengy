WITH pair_insertion AS (
    SELECT SUBSTR(LINE, 1, 2)                       AS from_pair,
           SUBSTR(LINE, 1, 1) || SUBSTR(LINE, 7, 1) AS to_pair
    FROM day14_input
    WHERE LINE LIKE '% -> %'
    UNION ALL
    SELECT SUBSTR(LINE, 1, 2)                       AS from_pair,
           SUBSTR(LINE, 7, 1) || SUBSTR(LINE, 2, 1) AS to_pair
    FROM day14_input
    WHERE LINE LIKE '% -> %'
),
     polymer(step, pair_totals) AS (
         WITH initial_polymer(pair, input) AS (
             SELECT ' ' || SUBSTR(LINE, 1, 1) AS pair,
                    LINE                      AS input
             FROM day14_input
             WHERE ROWID = 1
             UNION ALL
             SELECT SUBSTR(input, 1, 2) AS pair,
                    SUBSTR(input, 2)    AS input
             FROM initial_polymer
             WHERE LENGTH(input) > 1
         ),
              grouped_initial_polymer AS (
                  SELECT pair, COUNT(*) AS total
                  FROM initial_polymer
                  GROUP BY pair
              )
         SELECT 0 AS step,
                (SELECT GROUP_CONCAT(pair || '=' || total) || ',' FROM grouped_initial_polymer)
         UNION ALL
         SELECT polymer.step + 1 AS step,
                (
                    WITH pair_insertion AS (
                        SELECT SUBSTR(LINE, 1, 2)                       AS from_pair,
                               SUBSTR(LINE, 1, 1) || SUBSTR(LINE, 7, 1) AS to_pair
                        FROM day14_input
                        WHERE LINE LIKE '% -> %'
                        UNION ALL
                        SELECT SUBSTR(LINE, 1, 2)                       AS from_pair,
                               SUBSTR(LINE, 7, 1) || SUBSTR(LINE, 2, 1) AS to_pair
                        FROM day14_input
                        WHERE LINE LIKE '% -> %'
                    ),
                         pair(pair, total, input) AS (
                             SELECT SUBSTR(polymer.pair_totals, 1, 2) AS pair,
                                    1                                 AS total,
                                    SUBSTR(polymer.pair_totals, 6)    AS input
                             UNION ALL
                             SELECT SUBSTR(input, 1, 2)                                      AS pair,
                                    CAST(SUBSTR(input, 4, INSTR(input, ',') - 4) AS INTEGER) AS total,
                                    SUBSTR(input, INSTR(input, ',') + 1)                     AS input
                             FROM pair
                             WHERE input != ''
                         ),
                         grouped_pair AS (
                             SELECT IFNULL(pair_insertion.to_pair, pair.pair) AS pair, SUM(total) AS total
                             FROM pair
                                      LEFT JOIN pair_insertion ON pair_insertion.from_pair = pair.pair
                             GROUP BY IFNULL(pair_insertion.to_pair, pair.pair)
                         )
                    SELECT GROUP_CONCAT(pair || '=' || total) || ','
                    FROM grouped_pair
                )                AS pair_counts

         FROM polymer
         WHERE polymer.step < 40
     ),
     element(step, element, total, input) AS (
         SELECT step,
                SUBSTR(pair_totals, 2, 1)                                            AS element,
                CAST(SUBSTR(pair_totals, 4, INSTR(pair_totals, ',') - 4) AS INTEGER) AS total,
                SUBSTR(pair_totals, INSTR(pair_totals, ',') + 1)                     AS input
         FROM polymer
         UNION ALL
         SELECT step,
                SUBSTR(input, 2, 1)                                      AS pair,
                CAST(SUBSTR(input, 4, INSTR(input, ',') - 4) AS INTEGER) AS total,
                SUBSTR(input, INSTR(input, ',') + 1)                     AS input
         FROM element
         WHERE input != ''
     ),
     element_grouped AS (
         SELECT step, element, SUM(total) AS total
         FROM element
         GROUP BY step, element
     ),
     part1 AS (
         SELECT MAX(total) - MIN(total)
         FROM element_grouped
         WHERE step = 10
     ),
     part2 AS (
         SELECT MAX(total) - MIN(total)
         FROM element_grouped
         WHERE step = 40
     )

SELECT (SELECT * FROM part1) AS part1,
       (SELECT * FROM part2) AS part2
;
