CREATE TEMP TABLE day3_test_input
(
    line TEXT
);

INSERT INTO day3_test_input
VALUES ('00100'),
       ('11110'),
       ('10110'),
       ('10111'),
       ('10101'),
       ('01111'),
       ('00111'),
       ('11100'),
       ('10000'),
       ('11001'),
       ('00010'),
       ('01010');

CREATE TEMP VIEW day3 AS
WITH day3(rowid, bit, position, value)
         AS (
        SELECT rowid,
               CAST(SUBSTR(line, 1, 1) AS INTEGER) AS bit,
               LENGTH(line) - 1                    AS position,
               SUBSTR(line, 2)                     AS value
        FROM day3_input
        UNION ALL
        SELECT day3.rowid,
               CAST(SUBSTR(day3.value, 1, 1) AS INTEGER) AS bit,
               LENGTH(day3.value) - 1                    AS position,
               SUBSTR(day3.value, 2)                     AS value
        FROM day3
        WHERE day3.value != ''
    )
SELECT rowid, bit, position
FROM day3;

SELECT gamma * epsilon AS part1
FROM (
         SELECT SUM(bits.gamma_bit * (1 << bits.position))   AS gamma,
                SUM(bits.epsilon_bit * (1 << bits.position)) AS epsilon
         FROM (
                  SELECT CASE WHEN SUM(bit) > (SELECT COUNT(*) FROM day3_input) / 2 THEN 1 ELSE 0 END AS gamma_bit,
                         CASE WHEN SUM(bit) > (SELECT COUNT(*) FROM day3_input) / 2 THEN 0 ELSE 1 END AS epsilon_bit,
                         position
                  FROM day3
                  GROUP BY position
              ) AS bits
     );

SELECT (WITH part2(prefix)
                 AS (
        SELECT ''
        UNION ALL
        SELECT (
                   SELECT CASE COUNT(*)
                              WHEN 0 THEN '?'
                              WHEN 1 THEN line
                              ELSE part2.prefix ||
                                   iif(SUM(iif(SUBSTR(line, LENGTH(part2.prefix) + 1, 1) = '1', 1, -1)) >= 0, '1', '0')
                              END
                   FROM day3_input
                   WHERE day3_input.line LIKE part2.prefix || '%'
                     AND LENGTH(day3_input.line) > LENGTH(part2.prefix)
               )
        FROM part2
        WHERE prefix != '?'
    )
        SELECT SUM(day3.bit * (1 << day3.position))
        FROM part2
                 INNER JOIN day3_input ON line = prefix
                 INNER JOIN day3 ON day3.rowid = day3_input.rowid)
           *
       (WITH part2(prefix)
                 AS (
               SELECT ''
               UNION ALL
               SELECT (
                          SELECT CASE COUNT(*)
                                     WHEN 0 THEN '?'
                                     WHEN 1 THEN line
                                     ELSE part2.prefix ||
                                          iif(SUM(iif(SUBSTR(line, LENGTH(part2.prefix) + 1, 1) = '1', 1, -1)) >= 0,
                                              '0', '1')
                                     END
                          FROM day3_input
                          WHERE day3_input.line LIKE part2.prefix || '%'
                            AND LENGTH(day3_input.line) > LENGTH(part2.prefix)
                      )
               FROM part2
               WHERE prefix != '?'
           )
        SELECT SUM(day3.bit * (1 << day3.position))
        FROM part2
                 INNER JOIN day3_input ON line = prefix
                 INNER JOIN day3 ON day3.rowid = day3_input.rowid) AS part2
