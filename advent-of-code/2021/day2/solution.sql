CREATE TEMP VIEW day2 AS
SELECT rowid,
       SUBSTR(line, 0, INSTR(line, ' '))               AS direction,
       CAST(SUBSTR(line, INSTR(line, ' ')) AS INTEGER) AS value
FROM day2_input;

SELECT SUM(CASE
               WHEN day2.direction = 'forward' THEN value
               ELSE 0 END) *
       SUM(CASE
               WHEN day2.direction = 'up' THEN -value
               WHEN day2.direction = 'down' THEN value
               ELSE 0 END) AS part1
FROM day2;

WITH part2(current_rowid, aim, horizontal, depth)
         AS (
        SELECT 0 AS current_rowid,
               0 AS aim,
               0 AS horizontal,
               0 AS depth
        UNION ALL
        SELECT day2.rowid              AS current_rowid,
               CASE
                   WHEN day2.direction = 'down' THEN aim + day2.value
                   WHEN day2.direction = 'up' THEN aim - day2.value
                   ELSE aim END        AS aim,
               CASE
                   WHEN day2.direction = 'forward' THEN horizontal + day2.value
                   ELSE horizontal END AS horizontal,
               CASE
                   WHEN day2.direction = 'forward' THEN depth + day2.value * aim
                   ELSE depth END      AS horizontal
        FROM part2
                 INNER JOIN day2 ON day2.rowid = current_rowid + 1
    )
SELECT horizontal * depth AS part2
FROM part2
WHERE part2.current_rowid = (SELECT COUNT(*) FROM day2);
