CREATE TEMP TABLE bingo_numbers AS
WITH bingo_numbers(i, number, input) AS (
    SELECT 0                                                  AS i,
           CAST(SUBSTR(line, 0, INSTR(line, ',')) AS INTEGER) AS number,
           SUBSTR(line, INSTR(line, ',') + 1) || ','          AS input
    FROM day4_input
    WHERE rowid = 1
    UNION ALL
    SELECT i + 1                                                AS i,
           CAST(SUBSTR(input, 0, INSTR(input, ',')) AS INTEGER) AS number,
           SUBSTR(input, INSTR(input, ',') + 1)                 AS input
    FROM bingo_numbers
    WHERE input != ''
)
SELECT i, number
FROM bingo_numbers;


CREATE TEMP TABLE range5 AS
WITH range(n) AS (
    SELECT 1
    UNION ALL
    SELECT n + 1
    FROM range
    WHERE n < 5
)
SELECT *
FROM range;

CREATE TEMP TABLE bingo_boards AS
SELECT (day4_input.rowid - 3) / 6                               AS board_id,
       (day4_input.rowid - 2) % 6                               AS board_row,
       range5.n                                                 AS board_column,
       CAST(SUBSTR(line, 1 + (range5.n - 1) * 3, 2) AS INTEGER) AS number
FROM day4_input
         CROSS JOIN range5
WHERE day4_input.rowid > 1
  AND line != '';

SELECT SUM(bingo_boards.number) *
       (SELECT number FROM bingo_numbers WHERE bingo_numbers.i = last_winning_board.i) AS part1
FROM (
         SELECT board_id,
                (
                    WITH bingo(n) AS (
                        SELECT 0 AS n
                        UNION ALL
                        SELECT n + 1 AS n
                        FROM bingo
                        WHERE (SELECT bingo_boards.board_id
                               FROM bingo_numbers
                                        CROSS JOIN bingo_boards
                               WHERE bingo_numbers.i < n
                                 AND bingo_numbers.number = bingo_boards.number
                                 AND boards.board_id = bingo_boards.board_id
                               GROUP BY board_id, board_row
                               HAVING COUNT(*) = 5
                               UNION ALL
                               SELECT board_id
                               FROM bingo_numbers
                                        CROSS JOIN bingo_boards
                               WHERE bingo_numbers.i < n
                                 AND bingo_numbers.number = bingo_boards.number
                                 AND boards.board_id = bingo_boards.board_id
                               GROUP BY board_id, board_column
                               HAVING COUNT(*) = 5) IS NULL
                    )
                    SELECT MAX(n) - 1
                    FROM bingo
                ) AS i
         FROM (SELECT board_id
               FROM bingo_boards
               GROUP BY board_id) AS boards
         GROUP BY TRUE
         HAVING MIN(i)
     ) AS last_winning_board
         INNER JOIN bingo_boards ON bingo_boards.board_id = last_winning_board.board_id
         LEFT JOIN bingo_numbers
                   ON bingo_numbers.i <= last_winning_board.i AND bingo_numbers.number = bingo_boards.number
WHERE bingo_numbers.number IS NULL;


SELECT SUM(bingo_boards.number) *
       (SELECT number FROM bingo_numbers WHERE bingo_numbers.i = last_winning_board.i) AS part2
FROM (
         SELECT board_id,
                (
                    WITH bingo(n) AS (
                        SELECT 0 AS n
                        UNION ALL
                        SELECT n + 1 AS n
                        FROM bingo
                        WHERE (SELECT bingo_boards.board_id
                               FROM bingo_numbers
                                        CROSS JOIN bingo_boards
                               WHERE bingo_numbers.i < n
                                 AND bingo_numbers.number = bingo_boards.number
                                 AND boards.board_id = bingo_boards.board_id
                               GROUP BY board_id, board_row
                               HAVING COUNT(*) = 5
                               UNION ALL
                               SELECT board_id
                               FROM bingo_numbers
                                        CROSS JOIN bingo_boards
                               WHERE bingo_numbers.i < n
                                 AND bingo_numbers.number = bingo_boards.number
                                 AND boards.board_id = bingo_boards.board_id
                               GROUP BY board_id, board_column
                               HAVING COUNT(*) = 5) IS NULL
                    )
                    SELECT MAX(n) - 1
                    FROM bingo
                ) AS i
         FROM (SELECT board_id
               FROM bingo_boards
               GROUP BY board_id) AS boards
         GROUP BY TRUE
         HAVING MAX(i)
     ) AS last_winning_board
         INNER JOIN bingo_boards ON bingo_boards.board_id = last_winning_board.board_id
         LEFT JOIN bingo_numbers
                   ON bingo_numbers.i <= last_winning_board.i AND bingo_numbers.number = bingo_boards.number
WHERE bingo_numbers.number IS NULL;
