CREATE TEMP TABLE fish AS
WITH fish(i, number, input) AS (
    SELECT 0                                                  AS i,
           CAST(SUBSTR(line, 0, INSTR(line, ',')) AS INTEGER) AS number,
           SUBSTR(line, INSTR(line, ',') + 1) || ','          AS input
    FROM day6_input
    WHERE rowid = 1
    UNION ALL
    SELECT i + 1                                                AS i,
           CAST(SUBSTR(input, 0, INSTR(input, ',')) AS INTEGER) AS number,
           SUBSTR(input, INSTR(input, ',') + 1)                 AS input
    FROM fish
    WHERE input != ''
)
SELECT i, number
FROM fish;

CREATE TEMP VIEW progression AS
WITH progression(day, n0, n1, n2, n3, n4, n5, n6, n7, n8) AS (
    SELECT 0                                            AS day,
           (SELECT COUNT(*) FROM fish WHERE number = 0) AS n0,
           (SELECT COUNT(*) FROM fish WHERE number = 1) AS n1,
           (SELECT COUNT(*) FROM fish WHERE number = 2) AS n2,
           (SELECT COUNT(*) FROM fish WHERE number = 3) AS n3,
           (SELECT COUNT(*) FROM fish WHERE number = 4) AS n4,
           (SELECT COUNT(*) FROM fish WHERE number = 5) AS n5,
           (SELECT COUNT(*) FROM fish WHERE number = 6) AS n6,
           (SELECT COUNT(*) FROM fish WHERE number = 7) AS n7,
           (SELECT COUNT(*) FROM fish WHERE number = 8) AS n8
    UNION ALL
    SELECT day + 1 AS day,
           n1      AS n0,
           n2      AS n1,
           n3      AS n2,
           n4      AS n3,
           n5      AS n4,
           n6      AS n5,
           n7 + n0 AS n6,
           n8      AS n7,
           n0      AS n8
    FROM progression
    WHERE day < 300
)
SELECT day, n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 AS total
FROM progression;

SELECT total AS part1
FROM progression
WHERE day = 80;

SELECT total AS part2
FROM progression
WHERE day = 256;