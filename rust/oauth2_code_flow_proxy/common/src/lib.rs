use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthorizationCode {
    pub code: String,
}

#[derive(Debug, PartialEq)]
pub enum HttpStatus {
    Continue,
    SwitchingProtocols,
    Processing,
    Ok,
    Created,
    Accepted,
    NonAuthoritativeInformation,
    NoContent,
    ResetContent,
    PartialContent,
    MultiStatus,
    AlreadyReported,
    ImUsed,
    MultipleChoices,
    MovedPermanently,
    Found,
    SeeOther,
    NotModified,
    UseProxy,
    TemporaryRedirect,
    PermanentRedirect,
    BadRequest,
    Unauthorized,
    PaymentRequired,
    Forbidden,
    NotFound,
    MethodNotAllowed,
    NotAcceptable,
    ProxyAuthenticationRequired,
    RequestTimeout,
    Conflict,
    Gone,
    LengthRequired,
    PreconditionFailed,
    PayloadTooLarge,
    UriTooLong,
    UnsupportedMediaType,
    RangeNotSatisfiable,
    ExpectationFailed,
    ImATeapot,
    MisdirectedRequest,
    UnprocessableEntity,
    Locked,
    FailedDependency,
    UpgradeRequired,
    PreconditionRequired,
    TooManyRequests,
    RequestHeaderFieldsTooLarge,
    UnavailableForLegalReasons,
    InternalServerError,
    NotImplemented,
    BadGateway,
    ServiceUnavailable,
    GatewayTimeout,
    HttpVersionNotSupported,
    VariantAlsoNegotiates,
    InsufficientStorage,
    LoopDetected,
    NotExtended,
    NetworkAuthenticationRequired,
    Custom { code: u16, title: String },
}

impl HttpStatus {
    fn from_code(code: u16) -> Self {
        use self::HttpStatus::*;
        match code {
            100 => Continue,
            101 => SwitchingProtocols,
            102 => Processing,
            200 => Ok,
            201 => Created,
            202 => Accepted,
            203 => NonAuthoritativeInformation,
            204 => NoContent,
            205 => ResetContent,
            206 => PartialContent,
            207 => MultiStatus,
            208 => AlreadyReported,
            226 => ImUsed,
            300 => MultipleChoices,
            301 => MovedPermanently,
            302 => Found,
            303 => SeeOther,
            304 => NotModified,
            305 => UseProxy,
            307 => TemporaryRedirect,
            308 => PermanentRedirect,
            400 => BadRequest,
            401 => Unauthorized,
            402 => PaymentRequired,
            403 => Forbidden,
            404 => NotFound,
            405 => MethodNotAllowed,
            406 => NotAcceptable,
            407 => ProxyAuthenticationRequired,
            408 => RequestTimeout,
            409 => Conflict,
            410 => Gone,
            411 => LengthRequired,
            412 => PreconditionFailed,
            413 => PayloadTooLarge,
            414 => UriTooLong,
            415 => UnsupportedMediaType,
            416 => RangeNotSatisfiable,
            417 => ExpectationFailed,
            418 => ImATeapot,
            421 => MisdirectedRequest,
            422 => UnprocessableEntity,
            423 => Locked,
            424 => FailedDependency,
            426 => UpgradeRequired,
            428 => PreconditionRequired,
            429 => TooManyRequests,
            431 => RequestHeaderFieldsTooLarge,
            451 => UnavailableForLegalReasons,
            500 => InternalServerError,
            501 => NotImplemented,
            502 => BadGateway,
            503 => ServiceUnavailable,
            504 => GatewayTimeout,
            505 => HttpVersionNotSupported,
            506 => VariantAlsoNegotiates,
            507 => InsufficientStorage,
            508 => LoopDetected,
            510 => NotExtended,
            511 => NetworkAuthenticationRequired,
            _ => Custom {
                code,
                title: "Unknown".to_string(),
            },
        }
    }

    fn code(&self) -> u16 {
        use self::HttpStatus::*;
        match self {
            Continue => 100,
            SwitchingProtocols => 101,
            Processing => 102,
            Ok => 200,
            Created => 201,
            Accepted => 202,
            NonAuthoritativeInformation => 203,
            NoContent => 204,
            ResetContent => 205,
            PartialContent => 206,
            MultiStatus => 207,
            AlreadyReported => 208,
            ImUsed => 226,
            MultipleChoices => 300,
            MovedPermanently => 301,
            Found => 302,
            SeeOther => 303,
            NotModified => 304,
            UseProxy => 305,
            TemporaryRedirect => 307,
            PermanentRedirect => 308,
            BadRequest => 400,
            Unauthorized => 401,
            PaymentRequired => 402,
            Forbidden => 403,
            NotFound => 404,
            MethodNotAllowed => 405,
            NotAcceptable => 406,
            ProxyAuthenticationRequired => 407,
            RequestTimeout => 408,
            Conflict => 409,
            Gone => 410,
            LengthRequired => 411,
            PreconditionFailed => 412,
            PayloadTooLarge => 413,
            UriTooLong => 414,
            UnsupportedMediaType => 415,
            RangeNotSatisfiable => 416,
            ExpectationFailed => 417,
            ImATeapot => 418,
            MisdirectedRequest => 421,
            UnprocessableEntity => 422,
            Locked => 423,
            FailedDependency => 424,
            UpgradeRequired => 426,
            PreconditionRequired => 428,
            TooManyRequests => 429,
            RequestHeaderFieldsTooLarge => 431,
            UnavailableForLegalReasons => 451,
            InternalServerError => 500,
            NotImplemented => 501,
            BadGateway => 502,
            ServiceUnavailable => 503,
            GatewayTimeout => 504,
            HttpVersionNotSupported => 505,
            VariantAlsoNegotiates => 506,
            InsufficientStorage => 507,
            LoopDetected => 508,
            NotExtended => 510,
            NetworkAuthenticationRequired => 511,
            Custom { code, .. } => code.to_owned(),
        }
    }

    fn title(&self) -> String {
        use self::HttpStatus::*;
        match self {
            Continue => "Continue".to_string(),
            SwitchingProtocols => "Switching Protocols".to_string(),
            Processing => "Processing".to_string(),
            Ok => "OK".to_string(),
            Created => "Created".to_string(),
            Accepted => "Accepted".to_string(),
            NonAuthoritativeInformation => "Non-Authoritative Information".to_string(),
            NoContent => "No Content".to_string(),
            ResetContent => "Reset Content".to_string(),
            PartialContent => "Partial Content".to_string(),
            MultiStatus => "Multi-Status".to_string(),
            AlreadyReported => "Already Reported".to_string(),
            ImUsed => "IM Used".to_string(),
            MultipleChoices => "Multiple Choices".to_string(),
            MovedPermanently => "Moved Permanently".to_string(),
            Found => "Found".to_string(),
            SeeOther => "See Other".to_string(),
            NotModified => "Not Modified".to_string(),
            UseProxy => "Use Proxy".to_string(),
            TemporaryRedirect => "Temporary Redirect".to_string(),
            PermanentRedirect => "Permanent Redirect".to_string(),
            BadRequest => "Bad Request".to_string(),
            Unauthorized => "Unauthorized".to_string(),
            PaymentRequired => "Payment Required".to_string(),
            Forbidden => "Forbidden".to_string(),
            NotFound => "Not Found".to_string(),
            MethodNotAllowed => "Method Not Allowed".to_string(),
            NotAcceptable => "Not Acceptable".to_string(),
            ProxyAuthenticationRequired => "Proxy Authentication Required".to_string(),
            RequestTimeout => "Request Timeout".to_string(),
            Conflict => "Conflict".to_string(),
            Gone => "Gone".to_string(),
            LengthRequired => "Length Required".to_string(),
            PreconditionFailed => "Precondition Failed".to_string(),
            PayloadTooLarge => "Payload Too Large".to_string(),
            UriTooLong => "URI Too Long".to_string(),
            UnsupportedMediaType => "Unsupported Media Type".to_string(),
            RangeNotSatisfiable => "Range Not Satisfiable".to_string(),
            ExpectationFailed => "Expectation Failed".to_string(),
            ImATeapot => "I'm a teapot".to_string(),
            MisdirectedRequest => "Misdirected Request".to_string(),
            UnprocessableEntity => "Unprocessable Entity".to_string(),
            Locked => "Locked".to_string(),
            FailedDependency => "Failed Dependency".to_string(),
            UpgradeRequired => "Upgrade Required".to_string(),
            PreconditionRequired => "Precondition Required".to_string(),
            TooManyRequests => "Too Many Requests".to_string(),
            RequestHeaderFieldsTooLarge => "Request Header Fields Too Large".to_string(),
            UnavailableForLegalReasons => "Unavailable For Legal Reasons".to_string(),
            InternalServerError => "Internal Server Error".to_string(),
            NotImplemented => "Not Implemented".to_string(),
            BadGateway => "Bad Gateway".to_string(),
            ServiceUnavailable => "Service Unavailable".to_string(),
            GatewayTimeout => "Gateway Timeout".to_string(),
            HttpVersionNotSupported => "HTTP Version Not Supported".to_string(),
            VariantAlsoNegotiates => "Variant Also Negotiates".to_string(),
            InsufficientStorage => "Insufficient Storage".to_string(),
            LoopDetected => "Loop Detected".to_string(),
            NotExtended => "Not Extended".to_string(),
            NetworkAuthenticationRequired => "Network Authentication Required".to_string(),
            Custom { title, .. } => title.to_string(),
        }
    }
}

impl Serialize for HttpStatus {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_u16(self.code())
    }
}

impl<'de> Deserialize<'de> for HttpStatus {
    fn deserialize<D>(deserializer: D) -> Result<HttpStatus, D::Error>
    where
        D: Deserializer<'de>,
    {
        Deserialize::deserialize(deserializer).map(HttpStatus::from_code)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProblemJson {
    status: HttpStatus,
    title: String,
    detail: Option<String>,
}

impl ProblemJson {
    pub fn new(status: HttpStatus) -> Self {
        Self {
            title: status.title(),
            status,
            detail: None,
        }
    }

    pub fn new_with_detail(status: HttpStatus, detail: String) -> Self {
        Self {
            title: status.title(),
            status,
            detail: Some(detail),
        }
    }

    pub fn new_with_title_and_detail(status: HttpStatus, title: String, detail: String) -> Self {
        Self {
            status,
            title,
            detail: Some(detail),
        }
    }
}

#[cfg(feature = "rocket")]
mod rocket {
    extern crate rocket as _rocket;
    use super::ProblemJson;
    use _rocket::http::Status;
    use _rocket::request::Request;
    use _rocket::response::{self, Responder, Response};
    use _rocket::serde::json::Json;

    impl<'r, 'o: 'r> Responder<'r, 'o> for ProblemJson {
        fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
            let status = Status {
                code: self.status.code(),
            };
            let mut build = Response::build();
            build.merge(Json(self).respond_to(req)?);
            build.status(status).ok()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_test::{assert_de_tokens_error, assert_tokens, Token};

    #[test]
    fn test_http_status_ser_de() {
        assert_tokens(&HttpStatus::Ok, &[Token::U16(200)]);
        assert_tokens(
            &HttpStatus::Custom {
                code: 123,
                title: "Unknown".to_string(),
            },
            &[Token::U16(123)],
        );
        assert_de_tokens_error::<HttpStatus>(
            &[Token::Str("x")],
            "invalid type: string \"x\", expected u16",
        );
        assert_de_tokens_error::<HttpStatus>(
            &[Token::I64(100_000)],
            "invalid value: integer `100000`, expected u16",
        );
        assert_de_tokens_error::<HttpStatus>(
            &[Token::I64(-1)],
            "invalid value: integer `-1`, expected u16",
        );
        assert_de_tokens_error::<HttpStatus>(
            &[Token::F64(200.0)],
            "invalid type: floating point `200`, expected u16",
        );
    }
}
