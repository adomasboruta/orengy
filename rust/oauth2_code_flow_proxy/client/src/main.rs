use common::AuthorizationCode;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;
use tokio::time::{sleep, Duration};
use url::form_urlencoded;
use url::Url;
use uuid::Uuid;

#[derive(Debug, StructOpt)]
#[structopt(about = "Example of how OAuth2 client could be implemented")]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// OAuth2 Code Flow proxy URL
    #[structopt(short, long, default_value = "http://localhost:8080")]
    proxy_url: Url,

    /// IdP OAuth2 Code Flow Authorization URL
    #[structopt(short, long, env = "AUTHORIZATION_URL")]
    authorization_url: Url,

    /// IdP OAuth2 Code Flow Token URL
    #[structopt(short, long, env = "TOKEN_URL")]
    token_url: Url,

    /// IdP Client ID
    #[structopt(long, env = "CLIENT_ID", hide_env_values = true)]
    client_id: String,

    /// IdP Client Secret
    #[structopt(long, env = "CLIENT_SECRET", hide_env_values = true)]
    client_secret: String,

    /// Audience that will be used when issuing token
    #[structopt(long, env = "AUDIENCE")]
    audience: Option<String>,

    /// Audience that will be used when issuing token
    #[structopt(long, env = "SCOPE")]
    scope: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TokenResponse {
    access_token: String,
    refresh_token: Option<String>,
    id_token: Option<String>,
    token_type: String,
    expires_in: u64,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt: Opt = Opt::from_args();
    let scopes = &["https://www.googleapis.com/auth/cloud-platform"];
    let authentication_manager = gcp_auth::init().await?;
    let token = authentication_manager.get_token(scopes).await?;

    let state = Uuid::new_v4().to_simple().to_string();
    if opt.debug {
        eprintln!("State: {:}", state);
    }

    let mut authorization_url: Url = opt.authorization_url.clone();
    let mut query = form_urlencoded::Serializer::new(String::new());
    query.append_pair("response_type", "code");
    query.append_pair("client_id", opt.client_id.as_str());
    query.append_pair("redirect_uri", opt.proxy_url.as_str());
    query.append_pair("state", state.as_str());
    if let Some(scope) = &opt.scope {
        query.append_pair("scope", scope);
    }
    if let Some(audience) = &opt.audience {
        query.append_pair("audience", audience);
    }
    authorization_url.set_query(Some(query.finish().as_str()));

    if opt.debug {
        eprintln!("Authorization url: {:}", authorization_url);
    }

    let mut u: Url = opt.proxy_url.clone();
    u.set_query(Some(
        form_urlencoded::Serializer::new(String::new())
            .append_pair("state", state.as_str())
            .append_pair("access_token", token.as_str())
            .append_pair("authorization_url", authorization_url.as_str())
            .finish()
            .as_str(),
    ));

    eprintln!("Proxy url: {:}", u);

    let get_code_url = opt.proxy_url.join(&format!("/api/codes/{}", state))?;
    let code = loop {
        let response = reqwest::get(get_code_url.as_str()).await?;
        match response.status() {
            StatusCode::OK => {
                let body: AuthorizationCode = response.json().await?;
                if opt.debug {
                    eprintln!("Authorization code: {}", &body.code)
                }
                break body.code;
            }
            StatusCode::NOT_FOUND => {
                if opt.debug {
                    eprintln!("Code not found yet, retrying")
                }
                sleep(Duration::from_secs(3)).await;
                continue;
            }
            _ => {
                eprintln!(
                    "Unknown response code while trying to get authorization code: {} responded with {:?}",
                    &get_code_url,
                    response.status()
                );
                eprintln!("{}", response.text().await?);
                std::process::exit(1);
            }
        }
    };

    let response = reqwest::Client::new()
        .post(opt.token_url.as_str())
        .form(&[
            ("grant_type", "authorization_code"),
            ("client_id", opt.client_id.as_str()),
            ("client_secret", opt.client_secret.as_str()),
            ("code", code.as_str()),
            ("redirect_uri", opt.proxy_url.as_str()),
        ])
        .send()
        .await?;

    if response.status() != StatusCode::OK {
        eprintln!(
            "Unknown response code while trying to get token: {} responded with {:?}",
            opt.token_url.as_str(),
            response.status()
        );
        eprintln!("{}", response.text().await?);
        std::process::exit(1);
    }

    let body: TokenResponse = response.json().await?;

    if opt.debug {
        eprintln!("{:?}", body);
    }
    println!("{}", body.access_token.as_str());
    Ok(())
}
