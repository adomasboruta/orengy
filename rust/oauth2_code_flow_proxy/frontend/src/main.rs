use common::AuthorizationCode;
use std::collections::HashMap;
use url::Url;
use wasm_bindgen::JsValue;
use wasm_bindgen_futures::spawn_local;
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, RequestMode, Window};

fn write_to_dom(window: &Window, message: &str) -> Result<(), JsValue> {
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    let val = document.create_element("p")?;
    val.set_text_content(Some(message));

    body.append_child(&val)?;
    Ok(())
}

fn main() -> Result<(), JsValue> {
    let window = web_sys::window().expect("no global `window` exists");
    let location = window.location();
    let href: String = location.href()?;
    let current_url = match Url::parse(href.as_str()) {
        Ok(url) => url,
        Err(error) => {
            return write_to_dom(
                &window,
                &format!("Invalid page url='{}': '{}'", href, error),
            )
        }
    };

    let local_storage = if let Some(local_storage) = window.local_storage()? {
        local_storage
    } else {
        return write_to_dom(&window, "Your browser does not support local storage");
    };

    let query: HashMap<String, String> = current_url.query_pairs().into_owned().collect();
    let state = if let Some(state) = query.get("state") {
        state
    } else {
        return write_to_dom(&window, "Missing state query parameter");
    };
    let local_storage_key = format!("state_{}", state);
    if let (Some(access_token), Some(authorization_url)) =
        (query.get("access_token"), query.get("authorization_url"))
    {
        local_storage.set_item(&local_storage_key, access_token)?;
        location.set_href(authorization_url)
    } else if let Some(code) = query.get("code") {
        let access_token = if let Some(access_token) = local_storage.get_item(&local_storage_key)? {
            access_token
        } else {
            return write_to_dom(&window, "Cannot find access token in local storage");
        };
        local_storage.remove_item(&local_storage_key)?;

        let body = AuthorizationCode {
            code: code.to_string(),
        };
        let body = match serde_json::to_string(&body) {
            Ok(body) => body,
            Err(error) => {
                return write_to_dom(&window, &format!("Cannot serialize body: '{}'", error))
            }
        };
        let mut opts = RequestInit::new();
        opts.method("PUT");
        opts.body(Some(&JsValue::from_str(&body)));
        opts.mode(RequestMode::Cors);
        let request = Request::new_with_str_and_init(&format!("/api/codes/{}", state), &opts)?;
        request.headers().set("Authorization", &access_token)?;

        let response = JsFuture::from(window.fetch_with_request_and_init(&request, &opts));
        spawn_local(async move {
            match response.await {
                Ok(_) => {
                    let _ = write_to_dom(
                        &window,
                        "Authentication was successful. Closing browser in 5s",
                    );
                    let _ = window.close();
                }
                Err(_) => {
                    let _ = write_to_dom(
                        &window,
                        "Failed to post authorization code to backend server",
                    );
                }
            }
        });
        Ok(())
    } else {
        write_to_dom(&window, "Unknown combination of query parameters")
    }
}
