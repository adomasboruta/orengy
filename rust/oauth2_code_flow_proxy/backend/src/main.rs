#[macro_use]
extern crate rocket;

use async_lock::RwLock;
use clap::arg_enum;
use common::{AuthorizationCode, HttpStatus, ProblemJson};
use rocket::request::Request;
use rocket::serde::json::Json;
use rocket::State;
use std::collections::HashMap;
use structopt::StructOpt;

#[get("/codes/<state>")]
async fn get_state(
    storage: &State<RwLock<Box<dyn Storage>>>,
    state: &str,
) -> Result<Json<AuthorizationCode>, ProblemJson> {
    let storage = storage.read().await;
    storage
        .get(state)
        .map(|code| Json(AuthorizationCode { code }))
}

#[put("/codes/<state>", data = "<authorization_code>")]
async fn put_state(
    storage: &State<RwLock<Box<dyn Storage>>>,
    state: &str,
    authorization_code: Json<AuthorizationCode>,
) -> Result<(), ProblemJson> {
    let mut storage = storage.write().await;
    storage.put(state.to_string(), authorization_code.code.to_string())
}

arg_enum! {
    #[derive(Debug)]
    enum StorageKind {
        Memory,
        Firestore,
    }
}

#[derive(Debug, StructOpt)]
#[structopt(about = "Backend for OAuth2 Code flow proxy")]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long, env = "DEBUG")]
    debug: bool,

    /// Choose storage kind for code tokens
    #[structopt(long, possible_values = & StorageKind::variants(), case_insensitive = true, env = "STORAGE_KIND")]
    storage_kind: StorageKind,
}

trait Storage: Send + Sync {
    fn put(&mut self, state: String, code: String) -> Result<(), ProblemJson>;
    fn get(&self, state: &str) -> Result<String, ProblemJson>;
}

struct MemoryStorage {
    storage: HashMap<String, String>,
}

impl Storage for MemoryStorage {
    fn put(&mut self, state: String, code: String) -> Result<(), ProblemJson> {
        self.storage.insert(state, code);
        Ok(())
    }

    fn get(&self, state: &str) -> Result<String, ProblemJson> {
        if let Some(code) = self.storage.get(state) {
            Ok(code.to_string())
        } else {
            Err(ProblemJson::new_with_detail(
                HttpStatus::NotFound,
                format!("Authorization code not found for state {:?}", state),
            ))
        }
    }
}

#[catch(404)]
fn not_found(req: &Request) -> ProblemJson {
    ProblemJson::new_with_detail(
        HttpStatus::NotFound,
        format!("Path '{}' is not valid", req.uri()),
    )
}

fn main() -> Result<(), rocket::Error> {
    let opt: Opt = Opt::from_args();

    let storage: Box<dyn Storage> = match opt.storage_kind {
        StorageKind::Memory => Box::new(MemoryStorage {
            storage: HashMap::new(),
        }),
        StorageKind::Firestore => {
            todo!("Implement firestore storage")
        }
    };

    rocket::async_main(async move {
        rocket::build()
            .manage(RwLock::new(storage))
            .mount("/api", routes![get_state, put_state])
            .register("/api", catchers![not_found])
            .launch()
            .await
    })
}
