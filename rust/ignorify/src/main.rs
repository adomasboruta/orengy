use anyhow::{Context, Result};
use std::io;
use structopt::StructOpt;

fn git_to_docker_ignore(input: &mut impl io::Read, output: &mut impl io::Write) -> Result<()> {
    let mut buffer = "".to_string();

    input
        .read_to_string(&mut buffer)
        .with_context(|| "read to string from input")?;
    let lines = buffer
        .lines()
        .map(|line| {
            if line.trim() == "" {
                "".to_string()
            } else if line.starts_with('#') {
                line.to_string()
            } else if line.starts_with("!/") {
                format!("!{}", line.to_string().chars().skip(2).collect::<String>())
            } else if line.starts_with('!') {
                format!(
                    "!**/{}",
                    line.to_string().chars().skip(1).collect::<String>()
                )
            } else if line.starts_with('/') {
                line.to_string().chars().skip(1).collect::<String>()
            } else {
                format!("**/{}", line)
            }
        })
        .collect::<Vec<String>>()
        .join("\n");
    output
        .write_all(format!(".git\n{}\n", lines).as_bytes())
        .with_context(|| "write to output")?;

    Ok(())
}

#[derive(Debug, StructOpt)]
#[structopt(about = "Convert .gitignore to .dockerignore")]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Only check that .dockerignore is up to date
    #[structopt(long)]
    check: bool,

    /// Path to .gitignore file
    #[structopt(long, default_value = ".gitignore")]
    gitignore_path: String,

    /// Path to .dockerignore file
    #[structopt(long, default_value = ".dockerignore")]
    dockerignore_path: String,
}

fn main() -> Result<()> {
    let opt: Opt = Opt::from_args();
    let gitignore_file = std::fs::File::open(&opt.gitignore_path)
        .with_context(|| format!("open gitignore file {:?}", &opt.gitignore_path))?;
    let mut input = std::io::BufReader::new(gitignore_file);

    if opt.check {
        let mut output: Vec<u8> = Vec::new();
        git_to_docker_ignore(&mut input, &mut output)
            .with_context(|| "convert git to docker ignore")?;
        let existing_result = std::fs::read(&opt.dockerignore_path).with_context(|| {
            format!("read from docker ignore file {:?}", &opt.dockerignore_path)
        })?;
        if output != existing_result {
            anyhow::bail!(
                ".dockerignore file {:?} is not up to date with .gitignore {:?}\nDifference:\n{}",
                &opt.dockerignore_path,
                &opt.gitignore_path,
                difference::Changeset::new(
                    std::str::from_utf8(&output)?,
                    std::str::from_utf8(&existing_result)?,
                    "\n"
                )
            );
        }
        eprintln!("All izz well");
        Ok(())
    } else {
        let dockerignore_file = std::fs::File::create(&opt.dockerignore_path)
            .with_context(|| format!("create dockerignore file {:?}", &opt.dockerignore_path))?;
        let mut output = std::io::BufWriter::new(dockerignore_file);
        git_to_docker_ignore(&mut input, &mut output)
            .with_context(|| format!("convert git to docker ignore {:?}", &opt.dockerignore_path))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn writes_gitignore_input_to_dockerignore_output() {
        let mut output: Vec<u8> = Vec::new();

        assert!(git_to_docker_ignore(
            &mut r"
# spaces

# comment
# not absolute
!/test
# not relative
!test
# absolute
/test
# relative
test
"
            .as_bytes(),
            &mut output
        )
        .is_ok());
        let result = std::str::from_utf8(&output).unwrap();
        assert_eq!(
            &result,
            &r".git

# spaces

# comment
# not absolute
!test
# not relative
!**/test
# absolute
test
# relative
**/test
"
        );
    }
}
