# syntax = docker/dockerfile:1.2
FROM rust:1.54-bullseye as builder
RUN rustup target add wasm32-unknown-unknown
RUN cd /usr/local/bin \
 && wget -qO- https://github.com/thedodd/trunk/releases/download/v0.13.1/trunk-x86_64-unknown-linux-gnu.tar.gz | tar -xzf-
RUN mkdir -p /app/target /dist
WORKDIR /app
COPY rust/oauth2_code_flow_proxy /app
RUN --mount=type=cache,target=/app/target \
    --mount=type=cache,target=/usr/local/cargo/registry \
    cargo --locked build --release --bin backend \
 && cd frontend && trunk build --release \
 && cp /app/target/release/backend /dist \
 && cp -r /app/frontend/dist /dist/frontend

FROM debian:bullseye-slim as backend
COPY --from=builder /dist/backend /usr/local/bin/backend
CMD ["backend", "--storage-kind", "memory"]

#FROM debian:bullseye-slim as frontend
#RUN curl -sL https://firebase.tools/bin/linux/latest -o /usr/local/bin/firebase && chmod +x /usr/local/bin/firebase
#WORKDIR /app
#COPY --from=builder /dist/frontend /app
