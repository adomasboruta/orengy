import datetime
import glob
import csv
from collections import defaultdict
from dataclasses import dataclass, asdict
from enum import Enum, auto
import json
from typing import Dict, List

import requests


class AutoName(Enum):
    def _generate_next_value_(name: str, start, count, last_values):
        return " ".join(name.split("_")).capitalize()


class Bank(AutoName):
    REVOLUT = auto()
    SEB = auto()
    LUMINOR = auto()


class Category(AutoName):
    pass


class GeneralCategory(Category):
    INTERNAL = auto()
    UNKNOWN = auto()
    CASH_EXCHANGE = auto()
    FAMILY_LOAN = auto()


class MoneyInCategory(Category):
    ADOMAS_SALARY = "Adomas' salary"
    SHARON_SALARY = "Sharon's salary"
    MONEY_FROM_FAMILY = auto()
    HOUSE_LOAN = auto()
    SHARON_BUSINESS = auto()
    SHOPPING_REFUND = auto()


class MoneyOutCategory(Category):
    GROCERIES = auto()
    UTILITIES = auto()
    LOCAL_TRANSPORTATION = auto()
    SHOPPING = auto()
    MONEY_TO_FAMILY = auto()
    HOUSE_LOAN_PAYBACK = auto()
    STUDENT_LOAN_PAYBACK = auto()
    HOUSE_BUILDING = auto()
    TRAVELING = auto()
    SHARON_BUSINESS_COST = auto()
    MEDICAL = auto()
    ENTERTAINMENT = auto()
    RESTAURANTS = auto()
    MISC = auto()


@dataclass
class Transaction:
    date: datetime.date
    amount: float
    notes: str
    category: Category
    bank: Bank
    raw_data: Dict[str, str]


start_date = datetime.date(2023, 3, 1)


def read_revolut(data: str, gbp_exchange_rates: Dict[str, float]):
    reader = csv.DictReader(data.splitlines())
    for row in reader:
        if row["State"] != "COMPLETED":
            continue
        date = datetime.date.fromisoformat(row["Completed Date"].split(" ")[0])
        if date < start_date:
            continue
        amount = float(row["Amount"]) - float(row["Fee"])
        if row["Currency"] == "GBP":
            d = date
            while True:
                try:
                    amount /= gbp_exchange_rates[d.isoformat()]
                    break
                except KeyError:
                    d -= datetime.timedelta(days=1)
        elif row["Currency"] != "EUR":
            raise Exception(f"Revolut unknown currency {row['Currency']}")

        category = GeneralCategory.UNKNOWN
        notes = row["Description"]

        if row["Description"] in ("To EUR Savings", "Payment from Boruta Adomas", "Payment from Adomas Boruta"):
            category = GeneralCategory.INTERNAL
        if row["Description"] in (
            "pildyk.lt",
            "bitwarden.com",
            "EE",
            "To UAB Elektroninių mokėjimų agentūra",
        ):
            category = MoneyOutCategory.UTILITIES
        if row["Description"] in (
            "Uab Duliuksa",
            "To Ramūnas Veiverys",
            "UAB Rent Park",
            "Senukai",
            "To UAB SCHOMBURG Baltic",
            "To UAB \"Kauno vandenys\"",
            "Ermitažas.lt",
            "Ermitažas",
            "DEPO",
            "Uab Mproducts",
            "To UAB Fulhaus",
            "To UAB \"Climpro\"",
            "To UAB \"OBER-HAUS\" nekilnojamas turtas",
            "Voltus",
            "Uab Beka",
            "EKOLIUMENAS, šviestuvai Kaune",
            "Lemora | Statybinės Medžiagos | Technikos g.",
            "To UAB 'Legnoline'",
            "Magres Baldai",
            "LEMONA electronics",
        ):
            category = MoneyOutCategory.HOUSE_BUILDING
        if row["Description"] in (
            "Rimi",
            "Parduotuve Bostonas",
            "Maxima",
            "Sp Asienmarkt",
            "IKI",
            "Išlaužo žuvis",
            "PROMO Cash&Carry",
            "Sainsbury's",
            "WHSmith",
            "Šilas",
            "Uab Lama",
        ):
            category = MoneyOutCategory.GROCERIES
        if row["Description"] in (
            "City Sport, Parduotuve, Sportland LT",
            "To UAB KESKO SENUKAI DIGITAL",
            "To MB Skolas",
            "Aelia Duty Free",
            "Baldu Centras Nemuno B",
            "Decathlon",
            "Archon Studio",
            "IKEA",
            "Eurokos, parduotuve, Kosmelita",
            "Varle",
            "To Adebim store",
            "Amazon",
        ):
            category = MoneyOutCategory.SHOPPING
        if row["Description"] in (
            "Saigon",
            "Manami",
            "Mercato Metropolitano",
            "Pure",
            "Gogo Pocha",
            "The Beer Emporium",
            "Paladar",
            "Hakata Ramen + Bar",
            "Burger King",
            "Kumeliuko Sapnas",
            "Velo Baras",
            "Sushi Express",
            "Skonis",
        ):
            category = MoneyOutCategory.RESTAURANTS
        if row["Description"] in (
            "Uab Eudenta",
            "Uab Artmedica",
            "Eurovaistinė",
            "Gintarinė vaistinė",
            "Camelia vaistinė",
            "Akiu Lazerines Chirurg",
            "Uab Pilenu Vaistine",
            "Uab Pilenu Klinika",
            "Reprodukcines Medicino",
            "Benu",
            "To GINTARINĖ VAISTINĖ UAB",
        ):
            category = MoneyOutCategory.MEDICAL
        if row["Description"] in ("To IEVA SIMANONE", "To Ieva Borutaite"):
            if date in (
                datetime.date(2023, 3, 20),
                datetime.date(2023, 4, 6),
                datetime.date(2023, 4, 14),
                datetime.date(2023, 4, 18),
            ):
                category = GeneralCategory.CASH_EXCHANGE
            if date == datetime.date(2023, 3, 1):
                category = MoneyOutCategory.SHOPPING
        if row["Description"] in (
            "To Liudas Stuina",
            "To LIUDAS STUNIA",
        ):
            if date in (
                datetime.date(2023, 3, 2),
                datetime.date(2023, 3, 10),
                datetime.date(2023, 3, 18),
                datetime.date(2023, 3, 20),
                datetime.date(2023, 3, 27),
                datetime.date(2023, 3, 31),
                datetime.date(2023, 4, 21),
                datetime.date(2023, 4, 26),
                datetime.date(2023, 5, 28),
                datetime.date(2023, 6, 12),
                datetime.date(2023, 6, 24),
            ):
                category = MoneyOutCategory.HOUSE_BUILDING
        if row["Description"] in (
            "Payment from Liudas Stuina",
        ):
            if date in (
                datetime.date(2023, 4, 13),
            ):
                category = MoneyInCategory.MONEY_FROM_FAMILY
        if row["Description"] == "To Paysera LT":
            if date == datetime.date(2023, 3, 3):
                category = MoneyOutCategory.ENTERTAINMENT
                notes = "Home world expo tickets"
        if row["Description"] in ("From RUTA BORUTIENE", "Payment from Rūta Borutienė"):
            if date in (
                datetime.date(2023, 4, 5),
            ):
                category = MoneyInCategory.MONEY_FROM_FAMILY
            if date in (
                datetime.date(2023, 5, 2),
            ):
                category = GeneralCategory.FAMILY_LOAN

        if row["Description"].lower() == "To RUTA BORUTIENE".lower():
            if date in (
                datetime.date(2023, 3, 3),
                datetime.date(2023, 3, 27),
                datetime.date(2023, 5, 10),
                datetime.date(2023, 6, 12),
                datetime.date(2023, 6, 29),
            ):
                category = MoneyOutCategory.UTILITIES
            if date in (
                datetime.date(2023, 3, 5),
                datetime.date(2023, 4, 26),
                datetime.date(2023, 6, 30),
            ):
                category = GeneralCategory.CASH_EXCHANGE
            if date in (
                datetime.date(2023, 6, 1),
            ):
                category = GeneralCategory.FAMILY_LOAN
            if date == datetime.date(2023, 4, 1):
                category = MoneyOutCategory.MONEY_TO_FAMILY
        if row["Description"] == "Revolut Ltd":
            if date in (
                datetime.date(2023, 3, 5),
                datetime.date(2023, 3, 13),
                datetime.date(2023, 3, 26),
                datetime.date(2023, 5, 8),
                datetime.date(2023, 5, 9),
                datetime.date(2023, 6, 9),
            ):
                category = GeneralCategory.INTERNAL
        if row["Description"] == "From IEVA SIMANONE":
            category = MoneyInCategory.MONEY_FROM_FAMILY
        if row["Description"] in (
            "Unipark",
            "Konsultacines Poliklin",
            "Kaunas Lagoon parking site",
            "Skulas Virbaliskiai",
            "Baltic Petroleum",
            "VIADA",
            "EMSI",
            "Kauno klinikų daugiaaukštė stovėjimo aikštelė",
        ) or (row["Description"] == "U A B" and date == datetime.date(2023, 6, 26)):
            category = MoneyOutCategory.LOCAL_TRANSPORTATION
        if row["Description"] in (
            "Refund from Mytheresa",
            "Payment from Soundium",
        ):
            category = MoneyInCategory.SHOPPING_REFUND
        if row["Description"] in (
            "FormatC",
            "To Proseka UAB",
            "Aruba.it",
        ):
            category = MoneyOutCategory.MISC
        if row["Description"] in ("PaperSeal Kaunas",):
            category = MoneyOutCategory.SHARON_BUSINESS_COST
        if row["Description"] in (
            "Ryanair",
            "Transport for London",
            "National Express",
            "Kaunas Airport",
            "Bb Megabus/sccl",
            "East Midlands Railway",
            "LOVEtheatre",
        ):
            category = MoneyOutCategory.TRAVELING
        if row["Description"] in (
            "To Xi Yang",
            "Cash at 171 Long Lane Borough",
        ):
            category = GeneralCategory.CASH_EXCHANGE
        if row["Description"] in ("Exchanged to GBP",):
            category = GeneralCategory.INTERNAL
        if row["Description"] in ("Personal loan",):
            category = MoneyInCategory.HOUSE_LOAN
        if row["Description"] in ("To Slc Receipts",):
            category = MoneyOutCategory.STUDENT_LOAN_PAYBACK

        yield Transaction(
            date=date,
            amount=amount,
            notes=notes,
            category=category,
            bank=Bank.REVOLUT,
            raw_data=row,
        )


def get_gbp_exchange_rates():
    try:
        with open("gbp_exchange_rates.json") as f:
            data = json.load(f)
    except FileNotFoundError:
        data = {}

    try:
        last_known_date = datetime.date.fromisoformat(max(data))
    except ValueError:
        last_known_date = start_date

    if last_known_date < datetime.date.today():
        response = requests.get("https://www.lb.lt/lt/currency/exportlist/", params={
            "csv": "1",
            "currency": "GBP",
            "ff": "1",
            "class": "Eu",
            "type": "day",
            "date_from_day": last_known_date.isoformat(),
            "date_to_day": datetime.date.today().isoformat(),
        })
        response.raise_for_status()
        for item in csv.DictReader(response.text.splitlines(), delimiter=";"):
            data[item["Data"]] = float(item["Santykis"].replace(",", "."))
        with open("gbp_exchange_rates.json", "w") as f:
            json.dump(data, f)
    return data


def read_seb(data: str):
    reader = csv.DictReader(data.splitlines()[1:], delimiter=";")
    for row in reader:
        if row["VALIUTA"] != "EUR":
            raise Exception(f"Unknown SEB currency {row['VALIUTA']}")
        date = datetime.date.fromisoformat(row["DATA"])
        if date < start_date:
            continue
        amount = float(row["SUMA"].replace(",", "."))
        if row["DEBETAS/KREDITAS"] == "D":
            amount *= -1
        notes = row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"] + "\n" + row["MOKĖJIMO PASKIRTIS"]
        category = GeneralCategory.UNKNOWN

        if row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"] in (
            "Adomas Boruta",
            "WANG SHURONG",
        ):
            category = GeneralCategory.INTERNAL

        if any((
            "SUMANU paslaugų planas" in row["MOKĖJIMO PASKIRTIS"],
            "TELIA LIETUVA, AB" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "AB ENERGIJOS SKIRSTYMO OPERATORIUS" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.UTILITIES

        if "Grynųjų išmokėjimas" in row["TRANSAKCIJOS TIPAS"]:
            category = GeneralCategory.CASH_EXCHANGE
        if any((
            "UAB BEKA" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "DEPO" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "SCANDEX" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "ERMITAZAS" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "KESKO SENUKAI" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "EKOLIUMENAS" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "JURASTA" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "STIKLITA UAB" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "Rekuperatoriu centras" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "ACCANTO KAUNAS" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "UAB \"Kertina plius\"" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "MB \"Spynos be raktų\"" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.HOUSE_BUILDING

        if any((
            "SKULAS" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "BALTIC PETROLEUM" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.LOCAL_TRANSPORTATION

        if any((
            "PROMO CASH&CARRY" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "RIMI" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "MAXIMA" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "HUNTER STRIKE" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "IKI" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "Maisto prekiu parduotu" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "Parduotuve Bostonas" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.GROCERIES

        if any((
            "Restoranas DIA" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "EXPRESS PIZZA" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "Devyni drakonai" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "RINGAUDU SASLYKINE" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "SAIGON" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.RESTAURANTS

        if any((
            "Varle UAB" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "UAB PASAULIO GELES" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.SHOPPING

        if any((
            "BENU" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "UAB Pilenu vaistine" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "GINTARINE VAISTINE" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "UAB Ave vita" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "UAB EUROVAISTINE" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.MEDICAL

        if any((
            "WWW.F1.COM" in row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
        )):
            category = MoneyOutCategory.ENTERTAINMENT

        if row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"] == "UAB PROBONAS":
            category = MoneyInCategory.ADOMAS_SALARY
        if row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"] == "RŪTA BORUTIENĖ":
            if date == datetime.date(2023, 4, 29):
                category = GeneralCategory.CASH_EXCHANGE

        if row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"] == "UAB KESKO SENUKAI DIGITAL":
            if date == datetime.date(2023, 3, 20):
                category = MoneyOutCategory.SHOPPING
            if date == datetime.date(2023, 3, 24):
                category = MoneyInCategory.SHOPPING_REFUND

        if "https://www.kilobaitas.lt" in notes:
            if date == datetime.date(2023, 6, 4):
                category = MoneyOutCategory.HOUSE_BUILDING

        if row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"] == "DPD Lietuva UAB":
            if date == datetime.date(2023, 4, 13):
                category = MoneyOutCategory.SHARON_BUSINESS_COST
        if "https://lpexpress.lt" in row["MOKĖJIMO PASKIRTIS"]:
            if date == datetime.date(2023, 4, 20):
                category = MoneyOutCategory.SHARON_BUSINESS_COST
        if "AKADEMIJOS PASTAS" in row["MOKĖJIMO PASKIRTIS"]:
            if date == datetime.date(2023, 4, 21):
                category = MoneyOutCategory.SHARON_BUSINESS_COST

        yield Transaction(
            date=date,
            amount=amount,
            notes=notes,
            category=category,
            bank=Bank.SEB,
            raw_data=row,
        )


def main():
    gbp_exchange_rates = get_gbp_exchange_rates()
    transactions: List[Transaction] = []

    for file in glob.glob("data/*.csv"):
        try:
            with open(file) as f:
                data = f.read()
        except UnicodeDecodeError:
            with open(file, encoding="cp1257") as f:
                data = f.read()
        if data.startswith("Type,Product,Started Date,Completed Date,Description,Amount,Fee,Currency,State,Balance"):
            print(file, "is Revolut")
            for transaction in read_revolut(data, gbp_exchange_rates):
                transactions.append(transaction)
        elif data.startswith('﻿"SĄSKAITOS  (LT'):
            print(file, "is SEB")
            for transaction in read_seb(data):
                transactions.append(transaction)
        elif data.startswith('"Operacijos/Balanso Tipas";"Data";"Laikas";"Suma";"Ekvivalentas";"C/D";'):
            print(file, "is Luminor")
        else:
            # print(data[:100])
            raise Exception(f"Unknown bank - {file}")

    unknown_transactions = []
    amount_by_category = defaultdict(float)

    for transaction in transactions:
        amount_by_category[str(transaction.category)] += transaction.amount
        if transaction.category is GeneralCategory.UNKNOWN:
            unknown_transactions.append(transaction)
        if transaction.amount > 0 and isinstance(transaction.category, MoneyOutCategory):
            raise Exception(f"Money out with positive amount: {transaction}")
        if transaction.amount < 0 and isinstance(transaction.category, MoneyInCategory):
            raise Exception(f"Money out with positive amount: {transaction}")
    if unknown_transactions:
        print(f"Missing category for {len(unknown_transactions)} transactions")
        print(f"Sample transactions:")
        print(json.dumps(asdict(unknown_transactions[0]), indent=2, default=str))
    print(json.dumps(amount_by_category, indent=2, default=str))


if __name__ == '__main__':
    main()
