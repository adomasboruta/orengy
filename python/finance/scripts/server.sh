#!/usr/bin/env bash

# SEE: https://bertvv.github.io/cheat-sheets/Bash.html
set -euox pipefail;

export BASE_URL="http://localhost:8000"

poetry run sanic finance.server --dev --host "0.0.0.0"
