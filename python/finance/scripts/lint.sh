#!/usr/bin/env bash

# SEE: https://bertvv.github.io/cheat-sheets/Bash.html
set -euox pipefail;

poetry check
poetry run mypy . --install-types --non-interactive
poetry run ruff check .
poetry run black --check --diff .
