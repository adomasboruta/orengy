import copy
import csv
import datetime
import enum
import functools
import os
from typing import Type

import msgspec
import requests

from finance.gocardless import Institution

internal_accounts = {
    "LT903250057001793242",  # Adomas Revolut
    "LT067044090102136963",  # Adomas SEB
    "LT874010051005638895",  # Adomas Luminor with Card
    "LT604010051005638896",  # Adomas Luminor for Loan
    "LT667044090102136950",  # Shurong SEB
    "GB97REVO00997020951106",  # Shurong Revolut
}


class ParentCategory(enum.StrEnum):
    MONEY_IN = "Money In"
    MONEY_OUT = "Money Out"
    OTHER = "Other"

    def amount_valid(self, amount: float):
        if self is ParentCategory.MONEY_OUT and amount > 0:
            return False
        if self is ParentCategory.MONEY_IN and amount < 0:
            return False
        return True


class Category(str, enum.Enum):
    @staticmethod
    def _generate_next_value_(name: str, start, count, last_values):
        return " ".join(name.split("_")).capitalize()

    def __new__(cls, *values):
        obj = str.__new__(cls, values[0])
        obj._value_ = values[0]
        obj.parent_category = values[1]
        search_keywords: tuple[str, ...] = values[2] if len(values) > 2 else tuple()
        obj.search_keywords = tuple(kw.casefold() for kw in search_keywords)
        obj.budget: float | None = values[3] if len(values) > 3 else None
        return obj

    # GeneralCategory
    INTERNAL = enum.auto(), ParentCategory.OTHER, (
        "Payment from Boruta Adomas",
        "Payment from Adomas Boruta",
        "Exchanged to GBP",
        "To EUR",
        "To Adomas Boruta",
        "From ADOMAS BORUTA",
        "Payment from Wang Shurong",
        "SHU RONG WANG",
        *internal_accounts,
    )
    UNKNOWN = enum.auto(), ParentCategory.OTHER
    CASH_EXCHANGE = enum.auto(), ParentCategory.OTHER, (
        "Grynųjų išmokėjimas",
        "To Xi Yang",
        "Cash at 171 Long Lane Borough",
    )
    FAMILY_LOAN = enum.auto(), ParentCategory.OTHER

    # MoneyInCategory
    ADOMAS_SALARY = "Adomas' salary", ParentCategory.MONEY_IN, (
        "DEEL-SALARY",
        "UAB Probonas",
    )
    SHARON_SALARY = "Sharon's salary", ParentCategory.MONEY_IN
    MONEY_FROM_FAMILY = enum.auto(), ParentCategory.MONEY_IN
    HOUSE_LOAN = enum.auto(), ParentCategory.MONEY_IN, (
        "Paskolos išdavimas",
    )
    SHARON_BUSINESS = enum.auto(), ParentCategory.MONEY_IN, (
        "Payment from Stripe Technology Europe Ltd",
        "Vinted",
    )
    SHOPPING_REFUND = enum.auto(), ParentCategory.MONEY_IN, (
        "Refund from Mytheresa",
        "Payment from Soundium",
        "Senukai",
        "Refund from www.ermitazas.lt"
    )
    COMPANY_EXPENSE = enum.auto(), ParentCategory.MONEY_IN, (
        "Payment from Hyperexponential Ltd",
    )
    OTHER_MONEY_IN = enum.auto(), ParentCategory.MONEY_IN

    # MoneyOutCategory
    INVESTMENT = enum.auto(), ParentCategory.MONEY_OUT, (
        "GOINDEX",
    ), 1500
    GROCERIES = enum.auto(), ParentCategory.MONEY_OUT, (
        "PROMO CASH&CARRY",
        "RIMI",
        "MAXIMA",
        "HUNTER STRIKE",
        "IKI",
        "Maisto prekiu parduotu",
        "Parduotuve Bostonas",
        "Sp Asienmarkt",
        "Išlaužo žuvis",
        "Sainsbury's",
        "WHSmith",
        "Šilas",
        "Uab Lama",
        "UAB Vilniaus alus",
        "UAB Zalioji gatve",
        "Lidl",
        "PIENO ZVAIGZDES",
        "L.CIORAITIENE",
        "Birzu duonos krautuve",
        "Carrefour",
        "Biedronka",
        "Alkava",
        "Tesco",
        "Sainsburys",
        "Zabka",
    ), 600
    UTILITIES = enum.auto(), ParentCategory.MONEY_OUT, (
        "SUMANU paslaugų planas",
        "TELIA LIETUVA, AB",
        "AB ENERGIJOS SKIRSTYMO OPERATORIUS",
        "pildyk.lt",
        "bitwarden",
        "^EE$",
        "UAB Elektroninių mokėjimų agentūra",
        "Microsoft",
        "Paradox",
        "Disney Plus",
        "UAB \"VYČIO KOMISARAI\"",
        "LIETUVOS DRAUDIMAS",
        "Kauno rajono savivaldybės admimnistracija",
        "Ee Topup",
        "Komunaliniai",
        "TELE2",
        "SAULĖS KAIMELIS",
        "Premium Repricing 1 plan fee",
    ), 500
    LOCAL_TRANSPORTATION = enum.auto(), ParentCategory.MONEY_OUT, (
        "SKULAS",
        "BALTIC PETROLEUM",
        "Unipark",
        "Konsultacines Poliklin",
        "Kaunas Lagoon parking site",
        "VIADA",
        "EMSI",
        "Kauno klinikų daugiaaukštė stovėjimo aikštelė",
        "KAUNO KLINIKU AIKSTELE",
        "ARV-AUTO",
        "https://www.ketbilietai.lt",
        "Ketbilietai",
        "VI REGITRA",
        "UAB STOVA",
        "Naviparking Poland Sp",
        "Orlen",
        "Kauno Kliniku Stovejim",
        "Parking Novotel",
        "CityBee",
        "Kauno Kliniku Stov",
    ), 150
    SHOPPING = enum.auto(), ParentCategory.MONEY_OUT, (
        "UAB SOMI NETWORKS",
        "Kidz One",
        "Boozt.com",
        "Lemona",
        "ECCO",
        "Optometrijos centras",
        "babor-spa.lt",
        "SEEDAHOLIC",
        "CREME DE LA CREME",
        "UAB ARKIETE",
        "Varle UAB",
        "UAB PASAULIO GELES",
        "City Sport, Parduotuve, Sportland LT",
        "To UAB KESKO SENUKAI DIGITAL",
        "To MB Skolas",
        "Aelia Duty Free",
        "Baldu Centras Nemuno B",
        "Decathlon",
        "Archon Studio",
        "IKEA",
        "Eurokos, parduotuve, Kosmelita",
        "Varle",
        "To Adebim store",
        "Amazon",
        "UAB METALO AMZIUS",
        "JYSK",
        "Japan Centre Group",
        "ROSSMANN",
        "KRINONA",
        "mimarket.lt",
        "ZARA HOME",
        "UAB ELECTRONIC TRADE",
        "SPORTLAND",
        "With Juno",
        "TOPO CENTRAS",
        "Lush",
        "LOOKFANTASTIC",
        "Pflanzmichg",
        "Cult Beauty",
        "Notino",
        "Baltona Duty Free Shop",
        "Asian House",
        "TK Maxx",
        "HalfPrice",
        "UNIQLO",
        "HOMEZONE",
        "Pirkinys iš Wywywu",
        "KRISTIANA",
        "TELE 2 SALONAS",
        "kilobaitas.lt",
        "SIMPLEA UAB",
    ), 400
    MONEY_TO_FAMILY = enum.auto(), ParentCategory.MONEY_OUT
    HOUSE_LOAN_PAYBACK = enum.auto(), ParentCategory.MONEY_OUT, (
        "Paskolos grąžinimas",
        "Palūkanos už paskolą",
    ), 1500
    STUDENT_LOAN_PAYBACK = enum.auto(), ParentCategory.MONEY_OUT, (
        "To Slc Receipts",
        "Student Loans Company",
        "Student Loan Repayment",
    )
    HOUSE_BUILDING = enum.auto(), ParentCategory.MONEY_OUT, (
        "UAB BEKA",
        "UAB \"Ekovandenys\"",
        "Eldeiromas",
        "https://www.lemona.lt",
        "FURNITANAS",
        "VALSTYBĖS ĮMONĖ REGISTRŲ CENTRAS",
        "DEPO",
        "SCANDEX",
        "ERMITAZAS",
        "EKOLIUMENAS",
        "JURASTA",
        "STIKLITA UAB",
        "Rekuperatoriu centras",
        "ACCANTO KAUNAS",
        "UAB \"Kertina plius\"",
        "MB \"Spynos be raktų\"",
        "Uab Duliuksa",
        "To Ramūnas Veiverys",
        "UAB Rent Park",
        "Senukai",
        "To UAB SCHOMBURG Baltic",
        "Kauno vandenys",
        "Ermitažas",
        "DEPO",
        "Uab Mproducts",
        "To UAB Fulhaus",
        "To UAB \"Climpro\"",
        "To UAB \"OBER-HAUS\" nekilnojamas turtas",
        "Voltus",
        "Uab Beka",
        "Lemora",
        "To UAB 'Legnoline'",
        "Magres Baldai",
        "LEMONA electronics",
        "ATERMA",
        "UAB \"Medienos era\"",
        "UAB Aquahome",
        "UAB Ekovandenys",
        "UAB Vividum",
        "https://www.shtorm.lt",
        "METALIJA.LT",
        "DANEKA",
        "JAUKURAI",
        "GERUNDA",
        "GABESTA",
        "Uab Ulmas Sandelis",
        "NAVASAITIENĖ VAIDA",
        "SCHOMBURG BALTIC UAB",
        "Legnoline",
        "Registru Centro Kauno",
        "Bauen",
        "Medžiagos",
        "dažymas",
    )
    TRAVELING = enum.auto(), ParentCategory.MONEY_OUT, (
        "Ryanair",
        "Transport for London",
        "National Express",
        "Kaunas Airport",
        "Bb Megabus/sccl",
        "East Midlands Railway",
        "LOVEtheatre",
        "Wizz Air",
        "Arriva",
        "Kauno oro uostas",
        "RETRO Restauracja & Pensjonat",
        "Uber",
        "Booking.com",
        "AIRINN VILNIUS RECEPTI",
        "Greater Anglia",
        "TFL UNPAID FARE",
        "Tfl Travel Ch",
        "Megabus",
        "Sofitel",
        "LUFTHANSA",
        "lot.com",
        "East Mids Railway",
    ), 100
    SHARON_BUSINESS_COST = enum.auto(), ParentCategory.MONEY_OUT, (
        "PaperSeal",
        "Shopify",
        "Facebk",
        "Google Ads",
        "Vinted",
    )
    MEDICAL = enum.auto(), ParentCategory.MONEY_OUT, (
        "BENU",
        "UAB Pilenu vaistine",
        "GINTARINE VAISTINE",
        "UAB Ave vita",
        "UAB EUROVAISTINE",
        "Uab Eudenta",
        "Uab Artmedica",
        "Eurovaistinė",
        "Gintarinė vaistinė",
        "Camelia vaistinė",
        "Akiu Lazerines Chirurg",
        "Uab Pilenu Vaistine",
        "Uab Pilenu Klinika",
        "Reprodukcines Medicino",
        "Boots",
        "HERBARIUS",
        "PILENU SVEIKATOS PRIEZ",
        "LIETUVOS SVEIKATOS MOKSLŲ UNIVERSITETO LIGONINĖ KAUNO KLINIKOS",
        "www.gandrolabs.lt",
    ), 200
    ENTERTAINMENT = enum.auto(), ParentCategory.MONEY_OUT, (
        "WWW.F1.COM",
        "Forum Cinemas",
        "Palace Theatre",
        "UAB BOULINGO ZONA",
    ), 100
    RESTAURANTS = enum.auto(), ParentCategory.MONEY_OUT, (
        "Chew Fun London",
        "RESTORANAS",
        "BARAS",
        "SUGAMOUR",
        "Delici",
        "EXPRESS PIZZA",
        "Devyni drakonai",
        "RINGAUDU SASLYKINE",
        "SAIGON",
        "Manami",
        "Mercato Metropolitano",
        "Pure",
        "Gogo Pocha",
        "The Beer Emporium",
        "Paladar",
        "Hakata Ramen + Bar",
        "Burger King",
        "Kumeliuko Sapnas",
        "Velo Baras",
        "Sushi Express",
        "Skonis",
        "TROLIU PICA",
        "Pho Com",
        "Rice Coming Noodle Bar",
        "Pho & Bun",
        "Pekin Duck",
        "Murger Han City",
        "Old Compton Brasserie",
        "Starbucks",
        "The Ministry",
        "Arirang Restaurant",
        "Subway",
        "Viet Baguette",
        "Sol Y Sombra",
        "Costa Coffee",
        "HMSHost International",
        "Nikko Sushi",
        "Thai Me Up",
    ), 300
    MISC = enum.auto(), ParentCategory.MONEY_OUT, (
        "FormatC",
        "To Proseka UAB",
        "Aruba.it",
        "To Gonville&caiuspdd",
        "CloudFlare",
        "Linos artele",
        "Aruba Spa",
    ), 200

    @classmethod
    def detect(cls, search_index: str, transaction_amount: float) -> "Category":
        for c in cls:
            for kw in c.search_keywords:
                if kw in search_index and c.parent_category.amount_valid(transaction_amount):
                    return c
        return Category.UNKNOWN


class Currency(enum.StrEnum):
    EUR = "EUR"
    GBP = "GBP"


ExchangeRates = dict[Currency, dict[datetime.date, float]]


@functools.cache
def get_exchange_rates(today: datetime.date) -> ExchangeRates:
    start_date = datetime.date(2022, 12, 1)

    try:
        with open("exchange_rates.json") as f:
            data = msgspec.json.decode(f.read(), type=ExchangeRates)
    except FileNotFoundError:
        data = {}

    for currency in Currency:
        if currency is Currency.EUR:
            continue
        if currency not in data:
            data[currency] = {}

        try:
            last_known_date = max(data[currency])
        except ValueError:
            last_known_date = start_date

        if last_known_date < today:
            response = requests.get(
                "https://www.lb.lt/lt/currency/exportlist/",
                params={
                    "csv": "1",
                    "currency": currency,
                    "ff": "1",
                    "class": "Eu",
                    "type": "day",
                    "date_from_day": last_known_date.isoformat(),
                    "date_to_day": today.isoformat(),
                },
            )
            response.raise_for_status()
            for item in csv.DictReader(response.text.splitlines(), delimiter=";"):
                data[currency][datetime.date.fromisoformat(item["Data"])] = float(item["Santykis"].replace(",", "."))
    with open("exchange_rates.json", "wb") as f:
        f.write(
            msgspec.json.format(msgspec.json.encode(data), indent=2),
        )
    return data


class TransactionAmount(msgspec.Struct):
    amount: float
    currency: Currency

    def eur_amount(self, booking_date: datetime.date) -> float:
        if self.currency is Currency.EUR:
            return self.amount
        while True:
            try:
                return self.amount / get_exchange_rates(datetime.date.today())[self.currency][booking_date]
            except KeyError:
                booking_date -= datetime.timedelta(days=1)


class Account(msgspec.Struct):
    iban: str | None = None


class LuminorTransaction(msgspec.Struct, rename="camel"):
    booking_date: datetime.date
    value_date: datetime.date
    transaction_amount: TransactionAmount
    remittance_information_unstructured: str
    bank_transaction_code: str
    creditor_name: str | None = None
    creditor_account: Account | None = None
    debtor_name: str | None = None
    debtor_account: Account | None = None

    @property
    def other_side(self) -> str:
        return (self.creditor_name if self.transaction_amount.amount < 0 else self.debtor_name) or ""

    @property
    def search_index(self) -> str:
        return (self.description + "\n" + self.other_side).casefold()

    @property
    def description(self) -> str:
        return "\n".join(item for item in (
            self.remittance_information_unstructured,
        ) if item)

    def detect_category(self) -> Category:
        if self.transaction_amount.amount < 0 and self.creditor_account is not None and self.creditor_account.iban in internal_accounts:
            return Category.INTERNAL
        elif self.transaction_amount.amount > 0 and self.debtor_account is not None and self.debtor_account.iban in internal_accounts:
            return Category.INTERNAL
        elif self.bank_transaction_code.endswith("CWDL"):
            return Category.CASH_EXCHANGE
        elif self.creditor_name == "Luminor Bank AS Lietuvos skyrius" and self.transaction_amount.amount == -5.5:
            return Category.UTILITIES

        return Category.detect(
            self.search_index,
            self.transaction_amount.amount,
        )


class SebTransaction(msgspec.Struct, rename="camel"):
    booking_date: datetime.date
    value_date: datetime.date
    transaction_amount: TransactionAmount
    remittance_information_unstructured_array: list[str]
    bank_transaction_code: str
    remittance_information_unstructured: str = ""
    creditor_name: str | None = None
    creditor_account: Account | None = None
    debtor_name: str | None = None
    debtor_account: Account | None = None

    @property
    def other_side(self) -> str:
        return (self.creditor_name if self.transaction_amount.amount < 0 else self.debtor_name) or ""

    @property
    def search_index(self) -> str:
        return (self.description + "\n" + self.other_side).casefold()

    @property
    def description(self) -> str:
        return "\n".join(item for item in (
            self.remittance_information_unstructured,
            *self.remittance_information_unstructured_array,
        ) if item)

    def detect_category(self) -> Category:
        if self.transaction_amount.amount < 0 and self.creditor_account is not None and self.creditor_account.iban in internal_accounts:
            return Category.INTERNAL
        elif self.transaction_amount.amount > 0 and self.debtor_account is not None and self.debtor_account.iban in internal_accounts:
            return Category.INTERNAL
        elif self.bank_transaction_code.endswith("CWDL"):
            return Category.CASH_EXCHANGE

        return Category.detect(
            self.search_index,
            self.transaction_amount.amount
        )


class RevolutTransactionType(enum.StrEnum):
    TRANSFER = "TRANSFER"
    CARD_PAYMENT = "CARD_PAYMENT"
    TOPUP = "TOPUP"
    EXCHANGE = "EXCHANGE"
    ATM = "ATM"
    CARD_REFUND = "CARD_REFUND"
    LOAN = "LOAN"
    LOAN_PAYMENT = "LOAN_PAYMENT"
    FEE = "FEE"


class RevolutTransaction(msgspec.Struct, rename="camel"):
    booking_date: datetime.date
    value_date: datetime.date
    transaction_amount: TransactionAmount
    remittance_information_unstructured_array: list[str]
    proprietary_bank_transaction_code: RevolutTransactionType
    creditor_name: str | None = None
    creditor_account: Account | None = None
    debtor_name: str | None = None
    debtor_account: Account | None = None

    @property
    def other_side(self) -> str:
        return (self.creditor_name if self.transaction_amount.amount < 0 else self.debtor_name) or ""

    @property
    def search_index(self) -> str:
        return (self.description + "\n" + self.other_side).casefold()

    @property
    def description(self) -> str:
        return "\n".join(item for item in (
            *self.remittance_information_unstructured_array,
        ) if item)

    def detect_category(self) -> Category:
        if self.transaction_amount.amount < 0 and self.creditor_account is not None and self.creditor_account.iban in internal_accounts:
            return Category.INTERNAL
        elif self.transaction_amount.amount > 0 and self.debtor_account is not None and self.debtor_account.iban in internal_accounts:
            return Category.INTERNAL
        elif self.proprietary_bank_transaction_code is RevolutTransactionType.LOAN_PAYMENT:
            return Category.HOUSE_LOAN_PAYBACK
        elif self.proprietary_bank_transaction_code is RevolutTransactionType.LOAN:
            return Category.HOUSE_LOAN
        elif self.proprietary_bank_transaction_code is RevolutTransactionType.ATM:
            return Category.CASH_EXCHANGE

        return Category.detect(
            self.search_index,
            self.transaction_amount.amount
        )

class CashTransaction(msgspec.Struct, rename="camel"):
    date: datetime.date
    change: float
    balance: float
    reason: str
    category: Category

    @property
    def description(self) -> str:
        return self.reason

    def detect_category(self) -> Category:
        return self.category


class Transaction(msgspec.Struct):
    booking_date: datetime.date
    value_date: datetime.date
    transaction_amount: float
    description: str
    creditor_name: str
    debtor_name: str
    detected_category: Category
    category: Category | None = None

    @property
    def final_category(self) -> Category:
        return self.category or self.detected_category

    @property
    def other_side(self) -> str:
        return self.creditor_name if self.transaction_amount < 0 else self.debtor_name


def get_transactions(override_categories: dict[str, Category] | None = None):
    transactions: dict[str, Transaction]
    try:
        with open("new_transactions.yaml", "rb") as f:
            transactions = msgspec.yaml.decode(f.read(), type=dict[str, Transaction])
    except FileNotFoundError:
        transactions = {}

    for tid, transaction in detect_transactions():
        if tid in transactions:
            transaction.category = transactions[tid].category
        transactions[tid] = transaction

    if override_categories:
        for tid, category in override_categories.items():
            transactions[tid].category = category

    with open("new_transactions.yaml", "wb") as f:
        f.write(msgspec.yaml.encode(transactions))

    return transactions


def detect_transactions():
    for folder in os.listdir("transactions"):
        institution = Institution(folder)
        parse_type: Type
        if institution is Institution.REVOLUT_LT:
            parse_type = RevolutTransaction
        elif institution is Institution.REVOLUT_GB:
            parse_type = RevolutTransaction
        elif institution is Institution.SEB:
            parse_type = SebTransaction
        elif institution is Institution.LUMINOR:
            parse_type = LuminorTransaction
        elif institution is Institution.CASH:
            parse_type = CashTransaction
        else:
            raise NotImplementedError

        for file in os.listdir(f"transactions/{folder}"):
            with open(f"transactions/{folder}/{file}", "rb") as f:
                print(f"transactions/{folder}/{file}")
                transactions = msgspec.yaml.decode(f.read(), type=dict[str, parse_type], strict=False)
                for transaction_id, transaction in transactions.items():
                    if institution is Institution.CASH:
                        yield f"{institution}_{file.split('.')[0]}_{transaction_id}", Transaction(
                            booking_date=transaction.date,
                            value_date=copy.copy(transaction.date),
                            transaction_amount=transaction.change,
                            description=transaction.description,
                            creditor_name="",
                            debtor_name="",
                            detected_category=transaction.category,
                        )
                        continue

                    category = transaction.detect_category()
                    if not category.parent_category.amount_valid(transaction.transaction_amount.amount):
                        print(f"Invalid category for\n{msgspec.yaml.encode(transaction).decode()}")

                    yield f"{institution}_{file.split('.')[0]}_{transaction_id}", Transaction(
                        booking_date=transaction.booking_date,
                        value_date=transaction.value_date,
                        transaction_amount=transaction.transaction_amount.eur_amount(transaction.booking_date),
                        description=transaction.description,
                        creditor_name=transaction.creditor_name or "",
                        debtor_name=transaction.debtor_name or "",
                        detected_category=category,
                    )


if __name__ == '__main__':
    get_transactions()
    # for i in detect_transactions():
    #     print(i)
