import functools
import os
import time
from enum import StrEnum
from typing import Iterator

import jwt
from nordigen import NordigenClient
from nordigen.api import AccountApi
from nordigen.types import RequisitionList


@functools.cache
def client():
    return NordigenClient(
        secret_id=os.environ["NORDINGER_SECRET_ID"],
        secret_key=os.environ["NORDINGER_SECRET_KEY"],
    )


class Institution(StrEnum):
    REVOLUT_LT = "REVOLUT_REVOLT21"
    REVOLUT_GB = "REVOLUT_REVOGB21"
    LUMINOR = "LUMINOR_AGBLLT2X"
    SEB = "SEB_CBVILT2X"
    CASH = "CASH"


def update_token() -> bool:
    if not client().token:
        client().generate_token()
        return True
    expiration = jwt.decode(client().token, options={"verify_signature": False})["exp"]
    if time.time() + 60 > expiration:
        client().generate_token()
        return True
    return False


def account_apis() -> Iterator[tuple[str, AccountApi]]:
    update_token()
    limit = 100
    page = 0

    while True:
        requisitions: RequisitionList = client().requisition.get_requisitions(
            offset=page * limit,
            limit=limit,
        )

        for requisition in requisitions["results"]:
            if requisition["status"] == "EX":
                continue
            for account_id in requisition["accounts"]:
                account_api = client().account_api(account_id)
                yield requisition["institution_id"], account_api

        if not requisitions["results"]:
            break
        page += 1
