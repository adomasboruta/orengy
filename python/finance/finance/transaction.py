import copy
import csv
import datetime
import enum
import glob
from collections import defaultdict
from enum import auto
from typing import Iterable, Tuple

import msgspec
import requests
import xlsxwriter
import xlsxwriter.utility

start_date = datetime.date(2023, 3, 1)

internal_accounts = {
    "LT903250057001793242",  # Adomas Revolut
    "LT067044090102136963",  # Adomas SEB
    "LT874010051005638895",  # Adomas Luminor with Card
    "LT604010051005638896",  # Adomas Luminor for Loan
    "LT667044090102136950",  # Shurong SEB
}


class ParentCategory(enum.StrEnum):
    MONEY_IN = "Money In"
    MONEY_OUT = "Money Out"
    OTHER = "Other"

    def amount_valid(self, amount: float):
        if self is ParentCategory.MONEY_OUT and amount > 0:
            return False
        if self is ParentCategory.MONEY_IN and amount < 0:
            return False
        return True


class Category(str, enum.Enum):
    @staticmethod
    def _generate_next_value_(name: str, start, count, last_values):
        return " ".join(name.split("_")).capitalize()

    def __new__(cls, *values):
        obj = str.__new__(cls, values[0])
        obj._value_ = values[0]
        obj.parent_category = values[1]
        search_keywords: Tuple[str, ...] = values[2] if len(values) > 2 else tuple()
        obj.search_keywords = tuple(kw.casefold() for kw in search_keywords)
        obj.budget: float | None = values[3] if len(values) > 3 else None
        return obj

    # GeneralCategory
    INTERNAL = auto(), ParentCategory.OTHER, (
        "Payment from Boruta Adomas",
        "Payment from Adomas Boruta",
        "Exchanged to GBP",
        "To EUR",
        "To Adomas Boruta",
        "From ADOMAS BORUTA",
        "Payment from Wang Shurong",
        "SHU RONG WANG",
    )
    UNKNOWN = auto(), ParentCategory.OTHER
    CASH_EXCHANGE = auto(), ParentCategory.OTHER, (
        "Grynųjų išmokėjimas",
        "To Xi Yang",
        "Cash at 171 Long Lane Borough",
    )
    FAMILY_LOAN = auto(), ParentCategory.OTHER

    # MoneyInCategory
    ADOMAS_SALARY = "Adomas' salary", ParentCategory.MONEY_IN, (
        "DEEL-SALARY",
        "UAB Probonas",
    )
    SHARON_SALARY = "Sharon's salary", ParentCategory.MONEY_IN
    MONEY_FROM_FAMILY = auto(), ParentCategory.MONEY_IN, (
        "From IEVA SIMANONE",
    )
    HOUSE_LOAN = auto(), ParentCategory.MONEY_IN
    SHARON_BUSINESS = auto(), ParentCategory.MONEY_IN, (
        "Payment from Stripe Technology Europe Ltd",
        "Vinted",
    )
    SHOPPING_REFUND = auto(), ParentCategory.MONEY_IN, (
        "Refund from Mytheresa",
        "Payment from Soundium",
        "Senukai",
        "ERMITAŽAS"
    )
    COMPANY_EXPENSE = auto(), ParentCategory.MONEY_IN, (
        "Payment from Hyperexponential Ltd",
    )

    # MoneyOutCategory
    GROCERIES = auto(), ParentCategory.MONEY_OUT, (
        "PROMO CASH&CARRY",
        "RIMI",
        "MAXIMA",
        "HUNTER STRIKE",
        "IKI",
        "Maisto prekiu parduotu",
        "Parduotuve Bostonas",
        "Sp Asienmarkt",
        "Išlaužo žuvis",
        "Sainsbury's",
        "WHSmith",
        "Šilas",
        "Uab Lama",
        "UAB Vilniaus alus",
        "UAB Zalioji gatve",
        "Lidl",
        "PIENO ZVAIGZDES",
        "L.CIORAITIENE",
        "Birzu duonos krautuve",
        "Carrefour",
        "Biedronka",
    ), 600
    UTILITIES = auto(), ParentCategory.MONEY_OUT, (
        "SUMANU paslaugų planas",
        "TELIA LIETUVA, AB",
        "AB ENERGIJOS SKIRSTYMO OPERATORIUS",
        "pildyk.lt",
        "bitwarden.com",
        "^EE$",
        "UAB Elektroninių mokėjimų agentūra",
        "Microsoft",
        "Paradox",
        "Disney Plus",
        "UAB \"VYČIO KOMISARAI\"",
        "LIETUVOS DRAUDIMAS",
    ), 300
    LOCAL_TRANSPORTATION = auto(), ParentCategory.MONEY_OUT, (
        "SKULAS",
        "BALTIC PETROLEUM",
        "Unipark",
        "Konsultacines Poliklin",
        "Kaunas Lagoon parking site",
        "VIADA",
        "EMSI",
        "Kauno klinikų daugiaaukštė stovėjimo aikštelė",
        "KAUNO KLINIKU AIKSTELE",
        "ARV-AUTO",
        "https://www.ketbilietai.lt",
        "Ketbilietai",
        "VI REGITRA",
        "UAB STOVA",
        "Naviparking Poland Sp",
        "Orlen",
        "Kauno Kliniku Stovejim",
        "Parking Novotel",
        "CityBee",
    ), 150
    SHOPPING = auto(), ParentCategory.MONEY_OUT, (
        "babor-spa.lt",
        "CREME DE LA CREME",
        "UAB ARKIETE",
        "Varle UAB",
        "UAB PASAULIO GELES",
        "City Sport, Parduotuve, Sportland LT",
        "To UAB KESKO SENUKAI DIGITAL",
        "To MB Skolas",
        "Aelia Duty Free",
        "Baldu Centras Nemuno B",
        "Decathlon",
        "Archon Studio",
        "IKEA",
        "Eurokos, parduotuve, Kosmelita",
        "Varle",
        "To Adebim store",
        "Amazon",
        "UAB METALO AMZIUS",
        "JYSK",
        "Japan Centre Group",
        "ROSSMANN",
        "KRINONA",
        "mimarket.lt",
        "ZARA HOME",
        "UAB ELECTRONIC TRADE",
        "SPORTLAND",
        "With Juno",
        "TOPO CENTRAS",
        "Lush",
        "LOOKFANTASTIC",
        "Pflanzmichg",
        "Cult Beauty",
        "Notino",
        "Baltona Duty Free Shop",
        "Asian House",
        "TK Maxx",
        "HalfPrice",
        "UNIQLO",
        "HOMEZONE",
    ), 400
    MONEY_TO_FAMILY = auto(), ParentCategory.MONEY_OUT
    HOUSE_LOAN_PAYBACK = auto(), ParentCategory.MONEY_OUT, tuple(), 1800
    STUDENT_LOAN_PAYBACK = auto(), ParentCategory.MONEY_OUT, (
        "To Slc Receipts",
        "Student Loans Company",
    ), 830
    HOUSE_BUILDING = auto(), ParentCategory.MONEY_OUT, (
        "UAB BEKA",
        "https://www.lemona.lt",
        "FURNITANAS",
        "VALSTYBĖS ĮMONĖ REGISTRŲ CENTRAS",
        "DEPO",
        "SCANDEX",
        "ERMITAZAS",
        "EKOLIUMENAS",
        "JURASTA",
        "STIKLITA UAB",
        "Rekuperatoriu centras",
        "ACCANTO KAUNAS",
        "UAB \"Kertina plius\"",
        "MB \"Spynos be raktų\"",
        "Uab Duliuksa",
        "To Ramūnas Veiverys",
        "UAB Rent Park",
        "Senukai",
        "To UAB SCHOMBURG Baltic",
        "Kauno vandenys",
        "Ermitažas",
        "DEPO",
        "Uab Mproducts",
        "To UAB Fulhaus",
        "To UAB \"Climpro\"",
        "To UAB \"OBER-HAUS\" nekilnojamas turtas",
        "Voltus",
        "Uab Beka",
        "Lemora",
        "To UAB 'Legnoline'",
        "Magres Baldai",
        "LEMONA electronics",
        "ATERMA",
        "UAB \"Medienos era\"",
        "UAB Aquahome",
        "UAB Ekovandenys",
        "UAB Vividum",
        "https://www.shtorm.lt",
        "METALIJA.LT",
        "DANEKA",
        "JAUKURAI",
        "GERUNDA",
        "GABESTA",
        "Uab Ulmas Sandelis",
    )
    TRAVELING = auto(), ParentCategory.MONEY_OUT, (
        "Ryanair",
        "Transport for London",
        "National Express",
        "Kaunas Airport",
        "Bb Megabus/sccl",
        "East Midlands Railway",
        "LOVEtheatre",
        "Wizz Air",
        "Arriva",
        "Kauno oro uostas",
        "RETRO Restauracja & Pensjonat",
        "Uber",
        "Booking.com",
        "AIRINN VILNIUS RECEPTI",
        "Greater Anglia",
        "TFL UNPAID FARE",
        "Sofitel",
    ), 100
    SHARON_BUSINESS_COST = auto(), ParentCategory.MONEY_OUT, (
        "PaperSeal",
        "Shopify",
        "Facebk",
        "Google Ads",
        "Vinted",
    )
    MEDICAL = auto(), ParentCategory.MONEY_OUT, (
        "BENU",
        "UAB Pilenu vaistine",
        "GINTARINE VAISTINE",
        "UAB Ave vita",
        "UAB EUROVAISTINE",
        "Uab Eudenta",
        "Uab Artmedica",
        "Eurovaistinė",
        "Gintarinė vaistinė",
        "Camelia vaistinė",
        "Akiu Lazerines Chirurg",
        "Uab Pilenu Vaistine",
        "Uab Pilenu Klinika",
        "Reprodukcines Medicino",
        "Boots",
        "HERBARIUS",
        "PILENU SVEIKATOS PRIEZ",
        "LIETUVOS SVEIKATOS MOKSLŲ UNIVERSITETO LIGONINĖ KAUNO KLINIKOS",
    ), 200
    ENTERTAINMENT = auto(), ParentCategory.MONEY_OUT, (
        "WWW.F1.COM",
        "Forum Cinemas",
        "Palace Theatre",
    ), 100
    RESTAURANTS = auto(), ParentCategory.MONEY_OUT, (
        "Restoranas DIA",
        "EXPRESS PIZZA",
        "Devyni drakonai",
        "RINGAUDU SASLYKINE",
        "SAIGON",
        "Manami",
        "Mercato Metropolitano",
        "Pure",
        "Gogo Pocha",
        "The Beer Emporium",
        "Paladar",
        "Hakata Ramen + Bar",
        "Burger King",
        "Kumeliuko Sapnas",
        "Velo Baras",
        "Sushi Express",
        "Skonis",
        "TROLIU PICA",
        "Pho Com",
        "Rice Coming Noodle Bar",
        "Pho & Bun",
        "Pekin Duck",
        "Murger Han City",
        "Old Compton Brasserie",
        "Starbucks",
        "The Ministry",
        "Arirang Restaurant",
        "Subway",
        "Viet Baguette",
        "Sol Y Sombra",
        "Costa Coffee",
        "HMSHost International",
        "Nikko Sushi",
        "Thai Me Up",
    ), 200
    MISC = auto(), ParentCategory.MONEY_OUT, (
        "FormatC",
        "To Proseka UAB",
        "Aruba.it",
        "To Gonville&caiuspdd",
        "CloudFlare",
    ), 200


class RevolutTransactionType(enum.StrEnum):
    TRANSFER = "TRANSFER"
    CARD_PAYMENT = "CARD_PAYMENT"
    TOPUP = "TOPUP"
    EXCHANGE = "EXCHANGE"
    ATM = "ATM"
    CARD_REFUND = "CARD_REFUND"
    LOAN = "LOAN"
    LOAN_PAYMENT = "LOAN_PAYMENT"


class RevolutTransactionProduct(enum.StrEnum):
    SAVINGS = "Savings"
    CURRENT = "Current"


class RevolutTransactionCurrency(enum.StrEnum):
    EUR = "EUR"
    GBP = "GBP"


class RevolutTransactionState(enum.StrEnum):
    COMPLETED = "COMPLETED"


class RevolutTransaction(msgspec.Struct, tag=True):
    revolut_type: RevolutTransactionType
    product: RevolutTransactionProduct
    started_date: datetime.datetime
    completed_date: datetime.datetime
    description: str
    amount: float
    fee: float
    currency: RevolutTransactionCurrency
    state: RevolutTransactionState
    balance: float

    def detect_category(self) -> Category:
        if self.revolut_type is RevolutTransactionType.LOAN_PAYMENT:
            return Category.HOUSE_LOAN_PAYBACK
        elif self.revolut_type is RevolutTransactionType.LOAN:
            return Category.HOUSE_LOAN
        elif self.revolut_type is RevolutTransactionType.ATM:
            return Category.CASH_EXCHANGE
        elif self.product is RevolutTransactionProduct.SAVINGS:
            return Category.INTERNAL

        return Category.UNKNOWN


class SebTransactionCurrency(enum.StrEnum):
    EUR = "EUR"


class SebTransactionAccountCurrency(enum.StrEnum):
    EUR = "EUR"


class SebTransactionDebitCredit(enum.StrEnum):
    DEBIT = "D"
    CREDIT = "C"


class SebTransaction(msgspec.Struct, tag=True):
    document_number: str
    date: datetime.date
    currency: SebTransactionCurrency
    amount: float
    sender_or_receiver_name: str
    sender_or_receiver_id: str
    account_number: str
    financial_institution_name: str
    financial_institution_swift_code: str
    payment_purpose: str
    transaction_code: str
    document_date: datetime.date
    transaction_type: str
    link: str
    debit_credit: SebTransactionDebitCredit
    amount_in_account_currency: float
    my_account: str
    my_account_currency: SebTransactionAccountCurrency

    def detect_category(self) -> Category:
        if self.account_number in internal_accounts:
            return Category.INTERNAL
        return Category.UNKNOWN


class LuminorTransactionOperationOrBalanceType(enum.StrEnum):
    ATM_207 = "207"  # ATM
    TRANSACTION_C18 = "C18"  # Normal transaction
    INTERNAL_TRANSACTION_C45 = "C45"  # Internal transaction
    TRANSACTION_C68 = "C68"  # Normal transaction
    BANK_SUBSCRIPTION_E39 = "E39"  # Utility for bank account
    TRANSACTION_F26 = "F26"  # Normal transaction
    INTERNAL_TRANSACTION_F33 = "F33"  # Internal transaction initiated by luminor
    TRANSACTION_F40 = "F40"  # Normal transaction
    TRANSACTION_F43 = "F43"  # Online purchase
    ATM_COST_IA9 = "IA9"  # Cash withdrawal cost
    LOAN_RECEIVE_L01 = "L01"  # Loan receive, combine with operation_document_number
    LOAN_PAYMENT_L16 = "L16"  # Loan payment, combine with operation_document_number


class LuminorTransactionCreditOrDebit(enum.StrEnum):
    DEBIT = "D"
    CREDIT = "C"


class LuminorTransactionCurrency(enum.StrEnum):
    EUR = "EUR"


class LuminorTransaction(msgspec.Struct, tag=True):
    operation_or_balance_type: LuminorTransactionOperationOrBalanceType
    date: datetime.date
    time: int
    amount: float
    equivalent: float
    credit_or_debit: LuminorTransactionCreditOrDebit
    original_amount: float
    original_currency: LuminorTransactionCurrency
    operation_document_number: str
    operation_id: str
    client_code: str
    payment_code: str
    payment_purpose: str
    other_side_bic: str
    other_side_financial_institution_name: str
    other_side_account: str
    other_side_name: str
    other_side_personal_or_registration_number: str
    other_side_client_code: str
    origin_account: str
    origin_name: str
    origin_personal_or_registration_number: str
    destination_account: str
    destination_name: str
    destination_personal_or_registration_number: str

    def detect_category(self) -> Category:
        if self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.ATM_207:
            return Category.CASH_EXCHANGE
        elif any((
                self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.BANK_SUBSCRIPTION_E39,
                self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.ATM_COST_IA9,
        )):
            return Category.UTILITIES
        elif self.operation_document_number == "022-169619":
            if self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.LOAN_RECEIVE_L01:
                return Category.HOUSE_LOAN
            elif self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.LOAN_PAYMENT_L16:
                return Category.HOUSE_LOAN_PAYBACK
        elif any((
                self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.INTERNAL_TRANSACTION_F33,
                self.operation_or_balance_type is LuminorTransactionOperationOrBalanceType.INTERNAL_TRANSACTION_C45,
                self.other_side_account in internal_accounts,
        )):
            return Category.INTERNAL
        return Category.UNKNOWN


class CashTransaction(msgspec.Struct, tag=True):
    id: int
    date: datetime.date
    change: float
    balance: float
    reason: str
    category: Category


class Transaction(msgspec.Struct):
    date: datetime.date
    amount: float
    # notes: str
    category: Category
    raw_data: RevolutTransaction | SebTransaction | LuminorTransaction | CashTransaction

    def search_index(self) -> str:
        if isinstance(self.raw_data, RevolutTransaction):
            return "^" + "$^".join(item for item in (
                self.raw_data.revolut_type,
                self.raw_data.description,
            ) if item).casefold() + "$"
        elif isinstance(self.raw_data, SebTransaction):
            return "^" + "$^".join(item for item in (
                self.raw_data.sender_or_receiver_id,
                self.raw_data.sender_or_receiver_name,
                self.raw_data.payment_purpose,
                self.raw_data.financial_institution_name,
                self.raw_data.transaction_type,
                self.raw_data.link,
            ) if item).casefold() + "$"
        elif isinstance(self.raw_data, LuminorTransaction):
            return "^" + "$^".join(item for item in (
                self.raw_data.payment_purpose,
                self.raw_data.other_side_name,
            ) if item).casefold() + "$"


ExchangeRates = dict[RevolutTransactionCurrency, dict[datetime.date, float]]


def read_revolut(data: str, exchange_rates: ExchangeRates) -> Iterable[Tuple[str, Transaction]]:
    reader = csv.DictReader(data.splitlines())
    for row in reader:
        if not row["Completed Date"]:
            continue

        mapped_row = {
            "revolut_type": row["Type"],
            "product": row["Product"],
            "started_date": row["Started Date"],
            "completed_date": row["Completed Date"],
            "description": row["Description"],
            "amount": row["Amount"],
            "fee": row["Fee"],
            "currency": row["Currency"],
            "state": row["State"],
            "balance": row["Balance"],
        }

        try:
            t = msgspec.convert(mapped_row, type=RevolutTransaction, strict=False)
        except msgspec.ValidationError:
            print(row)
            raise

        if t.completed_date.date() < start_date:
            continue

        amount = t.amount - t.fee
        if t.currency is not RevolutTransactionCurrency.EUR:
            d = t.completed_date.date()
            while True:
                try:
                    amount /= exchange_rates[t.currency][d]
                    break
                except KeyError:
                    d -= datetime.timedelta(days=1)

        yield f"Revolut|{t.revolut_type}|{t.product}|{t.started_date}|{t.currency}|{t.description}", Transaction(
            date=t.completed_date.date(),
            amount=amount,
            category=t.detect_category(),
            raw_data=t,
        )


def read_seb(data: str) -> Iterable[Tuple[str, Transaction]]:
    reader = csv.DictReader(data.splitlines()[1:], delimiter=";")
    for row in reader:
        mapped_row = {
            "document_number": row["DOK NR."],
            "date": row["DATA"],
            "currency": row["VALIUTA"],
            "amount": float(row["SUMA"].replace(",", ".")),
            "sender_or_receiver_name": row["MOKĖTOJO ARBA GAVĖJO PAVADINIMAS"],
            "sender_or_receiver_id": row["MOKĖTOJO ARBA GAVĖJO IDENTIFIKACINIS KODAS"],
            "account_number": row["SĄSKAITA"],
            "financial_institution_name": row["KREDITO ĮSTAIGOS PAVADINIMAS"],
            "financial_institution_swift_code": row["KREDITO ĮSTAIGOS SWIFT KODAS"],
            "payment_purpose": row["MOKĖJIMO PASKIRTIS"],
            "transaction_code": row["TRANSAKCIJOS KODAS"],
            "document_date": row["DOKUMENTO DATA"],
            "transaction_type": row["TRANSAKCIJOS TIPAS"],
            "link": row["NUORODA"],
            "debit_credit": row["DEBETAS/KREDITAS"],
            "amount_in_account_currency": float(row["SUMA SĄSKAITOS VALIUTA"].replace(",", ".")),
            "my_account": row["SĄSKAITOS NR"],
            "my_account_currency": row["SĄSKAITOS VALIUTA"],
        }

        try:
            t = msgspec.convert(mapped_row, type=SebTransaction)
        except msgspec.ValidationError:
            print(row)
            raise

        if t.date < start_date:
            continue

        yield f"Seb|{t.transaction_code}", Transaction(
            date=copy.copy(t.date),
            amount=t.amount_in_account_currency * (-1 if t.debit_credit is SebTransactionDebitCredit.DEBIT else 1),
            category=t.detect_category(),
            raw_data=t,
        )


def read_luminor(data: str) -> Iterable[Tuple[str, Transaction]]:
    reader = csv.DictReader(data.splitlines(), delimiter=";")
    yield from ()
    for row in reader:
        mapped_row = {
            "operation_or_balance_type": row["Operacijos/Balanso Tipas"],
            "date": f"{row['Data'][0:4]}-{row['Data'][4:6]}-{row['Data'][6:8]}",
            "time": int(row["Laikas"]),
            "amount": row["Suma"],
            "equivalent": row["Ekvivalentas"],
            "credit_or_debit": row["C/D"],
            "original_amount": row["Orig. suma"],
            "original_currency": row["Orig. valiuta"],
            "operation_document_number": row["Operacijos dok. Nr."],
            "operation_id": row["Operacijos eilutė (identifikatorius)"],
            "client_code": row["Kliento kodas gavėjo informac. sistemoje"],
            "payment_code": row["Įmokos kodas"],
            "payment_purpose": row["Mokėjimo paskirtis"],
            "other_side_bic": row["Kitos pusės BIC"],
            "other_side_financial_institution_name": row["Kitos pusės Kredito įstaigos pavadinimas"],
            "other_side_account": row["Kitos pusės Sąskaitos Nr."],
            "other_side_name": row["Kitos pusės Pavadinimas"],
            "other_side_personal_or_registration_number": row["Kitos pusės Asmens kodas/Registracijos Nr."],
            "other_side_client_code": row["Kitos pusės Kliento kodas mokėtojo informacinėje sistemoje"],
            "origin_account": row["Pradinio mokėtojo Sąskaitos Nr."],
            "origin_name": row["Pradinio mokėtojo Vardas ir pavardė/Pavadinimas"],
            "origin_personal_or_registration_number": row["Pradinio mokėtojo Asmens kodas/Registracijos Nr."],
            "destination_account": row["Galutinio gavėjo Sąskaitos Nr."],
            "destination_name": row["Galutinio gavėjo Vardas ir pavardė/Pavadinimas"],
            "destination_personal_or_registration_number": row["Galutinio gavėjo Asmens kodas/Registracijos Nr."],
        }

        try:
            t = msgspec.convert(mapped_row, type=LuminorTransaction, strict=False)
        except msgspec.ValidationError:
            print(row)
            raise

        if t.date < start_date:
            continue

        yield f"Luminor|{t.operation_id}", Transaction(
            date=copy.copy(t.date),
            amount=t.amount * (-1 if t.credit_or_debit is LuminorTransactionCreditOrDebit.DEBIT else 1),
            category=t.detect_category(),
            raw_data=t,
        )


def read_cash(data: str) -> Iterable[Tuple[str, Transaction]]:
    reader = csv.DictReader(data.splitlines(), delimiter="\t")
    for row in reader:
        try:
            t = msgspec.convert(row, type=CashTransaction, strict=False)
        except msgspec.ValidationError:
            print(row)
            raise

        if t.date < start_date:
            continue

        yield f"Cash|{t.id}", Transaction(
            date=copy.copy(t.date),
            amount=t.change,
            category=t.category,
            raw_data=t,
        )


def get_exchange_rates() -> ExchangeRates:
    today = datetime.date.today()

    try:
        with open("exchange_rates.json") as f:
            data = msgspec.json.decode(f.read(), type=ExchangeRates)
    except FileNotFoundError:
        data = {}

    for currency in RevolutTransactionCurrency:
        if currency is RevolutTransactionCurrency.EUR:
            continue
        if currency not in data:
            data[currency] = {}

        try:
            last_known_date = max(data[currency])
        except ValueError:
            last_known_date = start_date

        if last_known_date < today:
            response = requests.get(
                "https://www.lb.lt/lt/currency/exportlist/",
                params={
                    "csv": "1",
                    "currency": "GBP",
                    "ff": "1",
                    "class": "Eu",
                    "type": "day",
                    "date_from_day": last_known_date.isoformat(),
                    "date_to_day": today.isoformat(),
                },
            )
            response.raise_for_status()
            for item in csv.DictReader(response.text.splitlines(), delimiter=";"):
                data[currency][datetime.date.fromisoformat(item["Data"])] = float(item["Santykis"].replace(",", "."))
    with open("exchange_rates.json", "wb") as f:
        f.write(
            msgspec.json.format(msgspec.json.encode(data), indent=2),
        )
    return data


def process_transactions() -> Iterable[Tuple[str, Transaction]]:
    exchange_rates = get_exchange_rates()
    for file in glob.glob("data/*.csv"):
        try:
            with open(file) as f:
                data = f.read()
        except UnicodeDecodeError:
            with open(file, encoding="cp1257") as f:
                data = f.read()
        if data.startswith("Type,Product,Started Date,Completed Date,Description,Amount,Fee,Currency,State,Balance"):
            print(file, "is Revolut")
            yield from read_revolut(data, exchange_rates)
        elif data.startswith('﻿"SĄSKAITOS  (LT'):
            print(file, "is SEB")
            yield from read_seb(data)
        elif data.startswith('"Operacijos/Balanso Tipas";"Data";"Laikas";"Suma";"Ekvivalentas";"C/D";'):
            print(file, "is Luminor")
            yield from read_luminor(data)
        elif data.startswith('id	date	change	balance	reason	category'):
            print(file, "is Cash")
            yield from read_cash(data)
        else:
            print(f"Unknown bank - {file}")


def main():
    transactions: dict[str, Transaction]
    try:
        with open("transactions.yaml", "rb") as f:
            transactions = msgspec.yaml.decode(f.read(), type=dict[str, Transaction])
    except FileNotFoundError:
        transactions = {}

    for tid, transaction in process_transactions():
        if tid in transactions:
            if transaction.raw_data != transactions[tid].raw_data:
                print(f"Conflicting transactions with the same id {tid}")
                print(
                    f"Existing transaction: {msgspec.json.format(msgspec.json.encode(transactions[tid])).decode()}")
                print(f"New transaction: {msgspec.json.format(msgspec.json.encode(transaction)).decode()}")
                raise Exception
            if transactions[tid].category is Category.UNKNOWN:
                transactions[tid].category = transaction.category
            continue
        transactions[tid] = transaction

    transactions = dict(sorted(transactions.items(), key=lambda item: (item[1].date, item[0])))

    known = 0
    amount_by_category: dict[str, dict[str, float]] = defaultdict(lambda: defaultdict(float))
    for transaction in transactions.values():
        if transaction.category is Category.UNKNOWN:
            search_index = transaction.search_index()
            for c in Category:
                for kw in c.search_keywords:
                    if kw in search_index and c.parent_category.amount_valid(transaction.amount):
                        transaction.category = c
                        break
                if transaction.category is not Category.UNKNOWN:
                    break

        if not transaction.category.parent_category.amount_valid(transaction.amount):
            print("==============")
            print(f"Invalid category for\n{msgspec.yaml.encode(transaction).decode()}")
            raise ValueError

        if transaction.category is not Category.UNKNOWN:
            known += 1
        amount_by_category[transaction.category.parent_category][transaction.category] += transaction.amount
    print("===================")
    print(f"Mapped data {known}/{len(transactions)} ({int(100 * known / len(transactions))}%)")
    print("===================")
    print(f"Amount by category\n\n{msgspec.yaml.encode(amount_by_category).decode()}")

    with open("transactions.yaml", "wb") as f:
        f.write(msgspec.yaml.encode(transactions))

    #####################################

    grouped_transactions: dict[str, dict[tuple[int, int], float]] = defaultdict(lambda: defaultdict(float))

    workbook = xlsxwriter.Workbook('report.xlsx')
    worksheet = workbook.add_worksheet("Summary")
    worksheet.freeze_panes(3, 3)

    bold = workbook.add_format({'bold': True})
    money_format = workbook.add_format({'num_format': '€#,###'})
    total_money_format = workbook.add_format({'bold': True, 'num_format': '€#,###', 'bg_color': "#b4c7dc"})
    over_budget_format = workbook.add_format({"font_color": "#9C0006"})
    date_format = workbook.add_format({'num_format': 'yyyy-mm-dd'})

    max_date = datetime.date.min
    min_date = datetime.date.max

    for transaction in transactions.values():
        min_date = min(min_date, transaction.date)
        max_date = max(max_date, transaction.date)
        grouped_transactions[transaction.category][
            (transaction.date.year, transaction.date.month)] += transaction.amount

    max_column = 3 + (max_date.year * 12 + max_date.month) - (min_date.year * 12 + min_date.month)

    months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ]

    year = min_date.year
    month = min_date.month
    worksheet.write(0, 0, f"From {min_date} to {max_date}")
    worksheet.write(2, 2, "Budget", bold)
    column = 3
    while year <= max_date.year and month <= max_date.month:
        if year == min_date.year and month == min_date.month or month == 1:
            worksheet.write(1, column, year, bold)
        worksheet.write(2, column, months[month - 1], bold)
        month += 1
        if month > 12:
            month -= 12
            year += 1
        column += 1

    row = 3
    for parent_category in ParentCategory:
        worksheet.write(row, 1, parent_category, bold)
        row += 1
        first_row = row
        column_sum = defaultdict(float)
        category_count = 0
        for category in Category:
            if category.parent_category != parent_category:
                continue
            worksheet.write(row, 1, category)
            if category.budget is not None:
                worksheet.write(row, 2, -category.budget, money_format)
                column_sum[2] += -category.budget
                worksheet.conditional_format(row, 2, row, max_column, {
                    'type': 'cell',
                    'criteria': '<',
                    'value': xlsxwriter.utility.xl_rowcol_to_cell(row, 2, row_abs=True, col_abs=True),
                    'format': over_budget_format,
                })

            category_count += 1

            for (year, month), amount in grouped_transactions[category].items():
                column = 3 + (year * 12 + month) - (min_date.year * 12 + min_date.month)
                worksheet.write(row, column, amount, money_format)
                column_sum[column] += amount

            row += 1
        worksheet.write(row, 1, "Total", total_money_format)
        for column in range(2, max_column + 1, 1):
            worksheet.write_formula(row, column,
                                    f"=SUM({xlsxwriter.utility.xl_range(first_row, column, row - 1, column)})",
                                    total_money_format, column_sum[column])
        row += 2

    worksheet.autofit()

    worksheet = workbook.add_worksheet("Transactions")
    worksheet.add_table(0, 0, len(transactions), 4, {
        'columns': [
            {'header': 'Date'},
            {'header': 'Category'},
            {'header': 'Amount'},
            {'header': 'Bank'},
            {'header': 'Raw Data'},
        ]
    })

    # worksheet.write(row + 1, 0, "Date", bold)
    # worksheet.write(row + 1, 1, "Category", bold)
    # worksheet.write(row + 1, 2, "Amount", bold)
    # worksheet.write(row + 1, 3, "Bank", bold)
    # worksheet.write(row + 1, 4, "Raw Data", bold)
    for row, transaction in enumerate(transactions.values()):
        worksheet.write(row + 1, 0, transaction.date, date_format)
        worksheet.write(row + 1, 1, transaction.category)
        worksheet.write(row + 1, 2, transaction.amount, money_format)
        worksheet.write(row + 1, 3, transaction.raw_data.__class__.__name__.replace("Transaction", ""))
        worksheet.write(row + 1, 4, msgspec.yaml.encode(transaction).decode().strip())

    worksheet.autofit()
    workbook.close()


if __name__ == "__main__":
    main()

# Totals
# Transactions by category and by month
# Add budget
# Highlight if budget is reached
