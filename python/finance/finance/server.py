import csv
import dataclasses
import datetime
import os
from collections import defaultdict
from uuid import uuid4

import msgspec
import sanic
from sanic import Sanic, redirect

from finance import gocardless, new_transactions

app = Sanic("Finance")

BASE_URL = os.environ["BASE_URL"]


@app.get("/debug")
async def debug(request):
    gocardless.update_token()
    result = []
    for _, account_api in gocardless.account_apis():
        result.append({
            "transactions": account_api.get_transactions(),
        })
    #     break

    return sanic.json(
        {
            "result": result,
            "requisitions": gocardless.client().requisition.get_requisitions(),
            "institutions": gocardless.client().institution.get_institutions("GB"),
        }
    )


@dataclasses.dataclass
class SummaryRowDTO:
    bold: bool
    name: str
    budget: float | str
    values: list[str | float]


@app.get("/")
@app.ext.template("index.html")
async def index(request):
    transactions = new_transactions.get_transactions()

    grouped_transactions: dict[str, dict[tuple[int, int], float]] = defaultdict(lambda: defaultdict(float))

    max_date = datetime.date.min
    min_date = datetime.date.max

    for transaction in transactions.values():
        min_date = min(min_date, transaction.booking_date)
        max_date = max(max_date, transaction.booking_date)
        grouped_transactions[transaction.final_category][
            (transaction.booking_date.year, transaction.booking_date.month)] += transaction.transaction_amount

    columns = (max_date.year * 12 + max_date.month) - (min_date.year * 12 + min_date.month) + 1

    rows: list[SummaryRowDTO] = []

    rows.append(SummaryRowDTO(
        bold=True,
        name="",
        budget="Budget",
        values=[
            str((min_date.year * 12 + min_date.month - 1 + i) // 12) +
            "-" +
            str((min_date.year * 12 + min_date.month - 1 + i) % 12 + 1)
            for i in range(columns)
        ]
    ))

    for parent_category in new_transactions.ParentCategory:
        rows.append(SummaryRowDTO(
            bold=False,
            name="",
            budget="",
            values=[""] * columns,
        ))
        rows.append(SummaryRowDTO(
            bold=True,
            name=parent_category,
            budget="",
            values=[""] * columns,
        ))

        column_sum = [0] * columns
        total_budget = 0
        for category in new_transactions.Category:
            if category.parent_category != parent_category:
                continue

            row = SummaryRowDTO(
                bold=False,
                budget=category.budget or "",
                name=category.value,
                values=[""] * columns
            )
            if category.budget:
                total_budget += category.budget

            for (year, month), amount in grouped_transactions[category].items():
                column = (year * 12 + month) - (min_date.year * 12 + min_date.month)
                row.values[column] = amount
                column_sum[column] += amount

            rows.append(row)
        rows.append(SummaryRowDTO(
            bold=True,
            name="Total",
            budget=total_budget or "",
            values=column_sum,
        ))

    return {
        "rows": rows,
    }


@app.get("/transactions")
@app.ext.template("transactions.html")
async def get_transactions(request: sanic.Request):
    date_from_raw = request.args.get("date_from", "")
    date_to_raw = request.args.get("date_to", "")
    category_raw = request.args.get("category", "")
    date_from = None
    date_to = None
    category = None

    if date_from_raw:
        date_from = datetime.date.fromisoformat(date_from_raw)
    if date_to_raw:
        date_to = datetime.date.fromisoformat(date_to_raw)
    if category_raw:
        category = new_transactions.Category(category_raw)

    transactions = new_transactions.get_transactions()
    results = []
    for tid, transaction in transactions.items():
        if date_from and date_from > transaction.booking_date:
            continue
        if date_to and date_to < transaction.booking_date:
            continue
        if category and category != transaction.final_category:
            continue
        results.append((tid, transaction))
    return {
        "transactions": sorted(results, key=lambda item: item[1].booking_date, reverse=True),
        "categories": new_transactions.Category,
        "filter": {
            "date_from": date_from_raw,
            "date_to": date_to_raw,
            "category": category_raw,
        }
    }


@app.post("/transactions")
async def update_transactions_categories(request: sanic.Request):
    override_categories = {}
    for tid in request.form.keys():
        override_categories[tid] = new_transactions.Category(request.form.get(tid))
    new_transactions.get_transactions(override_categories)
    return redirect(request.url, status=303)


@app.post("/fetch-transactions")
async def fetch_transactions(request: sanic.Request):
    for institution_id, account_api in gocardless.account_apis():
        os.makedirs(f"transactions/{institution_id}", exist_ok=True)
        try:
            account_details = account_api.get_details()["account"]
        except Exception as e:
            print(f"Failed to get account details: {e}")
            continue
        account_resource_id = account_details["resourceId"]
        file_name = f"transactions/{institution_id}/{account_resource_id}.yaml"
        transactions = {}
        try:
            with open(file_name, "rb") as f:
                transactions = msgspec.yaml.decode(f.read())
        except FileNotFoundError:
            pass

        for transaction in account_api.get_transactions()["transactions"]["booked"]:
            transaction_id = transaction.pop("transactionId")
            transactions[transaction_id] = transaction
        with open(file_name, "wb") as f:
            f.write(msgspec.yaml.encode(transactions))

    with open("data/cash_transactions.csv") as f:
        os.makedirs(f"transactions/CASH", exist_ok=True)
        reader = csv.DictReader(f, delimiter="\t")
        transactions = {}
        for row in reader:
            print(row)
            transaction_id = row.pop("id")
            transactions[transaction_id] = row
        with open(f"transactions/CASH/cash.yaml", "wb") as f:
            f.write(msgspec.yaml.encode(transactions))

    return redirect("/", status=303)


@app.post("/add-institution")
async def add_institution(request: sanic.Request):
    gocardless.update_token()

    institution_id = request.form.get("institution_id")

    init = gocardless.client().initialize_session(
        institution_id=institution_id,
        redirect_uri=BASE_URL,
        reference_id=str(uuid4()),
        max_historical_days=365,
    )
    return redirect(init.link, status=303)
