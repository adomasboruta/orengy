from diagrams import Diagram, Edge
from diagrams.onprem.client import User
from diagrams.onprem.compute import Server
from diagrams.programming.language import Python
from diagrams.saas.identity import Auth0


def main() -> None:
    with Diagram(
        "Initial design",
        show=False,
        filename="markdown/oauth2-code-flow-exchange/initial_design",
    ):
        exchange = Server("Exchange Server")
        idp = Auth0("Identity provider")
        consumer = Python("Consumer")
        user = User("User")

        consumer >> Edge(label="1. Authorization url") >> user
        (
            consumer
            >> Edge(label="2. Start waiting for authorization code")
            >> exchange
        )
        user >> Edge(label="3. Authenticate with identity provider") >> idp
        (
            idp
            >> Edge(label="4. Callback redirect with authorization code")
            >> exchange
        )
        exchange >> Edge(label="5. Respond with authorization code") >> consumer


if __name__ == "__main__":
    main()
