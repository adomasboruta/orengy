import os
import random
import string
import urllib.parse

import requests

# Application config
CLIENT_ID = os.environ["CLIENT_ID"]
CLIENT_SECRET = os.environ["CLIENT_SECRET"]
TOKEN_URL = os.environ["TOKEN_URL"]
AUTHORIZATION_URL = os.environ["AUTHORIZATION_URL"]
PROXY_URL = os.environ["PROXY_URL"]


def main() -> None:
    # Passing in state will allow matching long polling request with callback
    state = "".join(
        random.choices(string.ascii_uppercase + string.digits, k=32)
    )
    # Redirect/Callback URL to the proxy server
    redirect_url = f"{PROXY_URL}/oauth2/callback"
    # Standard parameters for code flow. You may need to add scope or audience,
    # depending on your identity provider
    params = {
        "client_id": CLIENT_ID,
        "redirect_uri": redirect_url,
        "response_type": "code",
        "state": state,
    }

    print("Please click on a link to authorize")
    print(f"{AUTHORIZATION_URL}?{urllib.parse.urlencode(params)}")

    # Start waiting for the callback request, pass in state to do matching
    response = requests.post(
        f"{PROXY_URL}/wait-for-callback", params={"state": state}
    )
    response.raise_for_status()
    # Get authorization code from the response
    authorization_code = response.json()["code"]

    # Use authorization code to get access token
    response = requests.post(
        TOKEN_URL,
        # More parameters may be needed, depending on your identity provider
        data={
            "grant_type": "authorization_code",
            "redirect_uri": redirect_url,
            "code": authorization_code,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
        },
    )
    response.raise_for_status()
    # Get access token. You may want to get other info depending on your
    # identity provider and situation
    access_token = response.json()["access_token"]
    print("Your access token")
    print(access_token)


if __name__ == "__main__":
    main()
