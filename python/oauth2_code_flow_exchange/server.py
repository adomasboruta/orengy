import asyncio
import json
from typing import Dict, Optional, TypeVar, Generic

from sanic import Sanic, response
from sanic.exceptions import SanicException
from sanic.request import Request
from sanic.response import HTTPResponse

app = Sanic("OAuth2 Authorization code flow exchange")

T = TypeVar("T")


class WaitableValue(Generic[T]):
    """
    Helper class to associate a value with asyncio.Event. This allows
    waiting for the value to be available
    """

    def __init__(self) -> None:
        self._event: asyncio.Event = asyncio.Event()
        self._value: Optional[T] = None

    async def wait(self, timeout: float) -> bool:
        """
        Wait for the value to be ready. Return if the value is set.
        """
        try:
            await asyncio.wait_for(self._event.wait(), timeout)
        except asyncio.TimeoutError:
            pass
        return self._event.is_set()

    def get(self) -> Optional[T]:
        return self._value

    def set(self, value: T) -> None:
        self._value = value
        self._event.set()


# In memory storage for keeping mappings
storage: Dict[str, WaitableValue[str]] = {}


def get_or_create_authorization_code(state: str) -> WaitableValue[str]:
    """
    Retrieve authorization code from storage. If it does not exist, create it
    and add to storage before returning.
    """
    try:
        return storage[state]
    except KeyError:
        value: WaitableValue[str] = WaitableValue()
        storage[state] = value
        return value


@app.post("/wait-for-callback")
async def wait_for_callback(request: Request) -> HTTPResponse:
    """
    Wait for the authorization code to be available. Use state parameter to map
    between this long polling request and callback from identity provider
    """
    # Get and validate state
    state: Optional[str] = request.args.get("state")
    if not state:
        raise SanicException("Missing state parameter", 400)
    authorization_code = get_or_create_authorization_code(state)

    # Start streaming response
    streaming_response: HTTPResponse = await request.respond(
        content_type="application/json"
    )
    # Send space character every 4 seconds. This is to make sure that
    # connection is kept open while we are waiting for callback to finish.
    # Various proxies and loadbalancers have tendency to kill the connection
    # if no data is exchanged for some time. Sending space will not affect the
    # response because we are using JSON.
    while not await authorization_code.wait(4):
        await streaming_response.send(b" ")
    # Respond with authorization code
    await streaming_response.send(
        json.dumps({"code": authorization_code.get()}).encode()
    )

    # Finish streaming
    await streaming_response.send(end_stream=True)
    return streaming_response


@app.get("/oauth2/callback")
async def callback(request: Request) -> HTTPResponse:
    """
    Store authorization code for particular state
    """
    # Get and validate state
    state: Optional[str] = request.args.get("state")
    if not state:
        raise SanicException("Missing state parameter", 400)
    authorization_code = get_or_create_authorization_code(state)

    # Get and validate state
    if authorization_code.get() is not None:
        raise SanicException(
            "Authorization code already received for this state",
            400,
        )

    # Get and validate authorization code
    code: Optional[str] = request.args.get("code")
    if not code:
        raise SanicException("Missing authorization code parameter", 400)

    # Store authorization code
    authorization_code.set(code)

    # Write a nice message to the user saying that authentication was successful
    return response.html(
        """
<html>
<head>
</head>
<body>
You have logged in. You can close this page and go back to your application
</body>
</html>
"""
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
