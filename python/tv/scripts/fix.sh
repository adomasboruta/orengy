#!/usr/bin/env bash

# SEE: https://bertvv.github.io/cheat-sheets/Bash.html
set -euox pipefail;

poetry run ruff check . --fix
poetry run ruff format .
