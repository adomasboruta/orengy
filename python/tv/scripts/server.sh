#!/usr/bin/env bash

# SEE: https://bertvv.github.io/cheat-sheets/Bash.html
set -euox pipefail;

poetry run sanic tv.server --dev --host "0.0.0.0"
