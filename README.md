# orengy

## Terraform

- Use [tfenv](https://github.com/tfutils/tfenv) to manage terraform version
- Use [tflint](https://github.com/terraform-linters/tflint) for terraform linting

## Python

- Use [poetry](https://github.com/python-poetry/poetry) to manage python dependencies

## Node

- Use [fnm](https://github.com/Schniz/fnm) to manage nodejs version
