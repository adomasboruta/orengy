output "tf_state_bucket_name" {
  value = "orengy-terraform-state"
}

output "google_region" {
  value = "europe-central2"
}

output "google_project" {
  value = "orengy"
}
