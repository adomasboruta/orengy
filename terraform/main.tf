module "common" {
  source = "./common"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google",
      version = "3.84.0",
    }
    google-beta = {
      source  = "hashicorp/google-beta",
      version = "3.84.0",
      configuration_aliases = [
        google-beta.firebase
      ]
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }

  backend "gcs" {
    bucket = "orengy-terraform-state"
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

provider "google" {
  project = module.common.google_project
  region  = module.common.google_region
}

provider "google-beta" {
  project = module.common.google_project
  region  = module.common.google_region
}

provider "google-beta" {
  alias        = "firebase"
  project      = module.common.google_project
  region       = module.common.google_region
  access_token = jsondecode(file("~/.config/configstore/firebase-tools.json"))["tokens"]["access_token"]
}

data "google_billing_account" "acct" {
  display_name = "Lithuania Billing account"
  open         = true
}

resource "google_project" "default" {
  name            = "Orengy"
  project_id      = module.common.google_project
  billing_account = data.google_billing_account.acct.id
}

resource "google_app_engine_application" "app" {
  project       = google_project.default.project_id
  location_id   = "europe-west"
  database_type = "CLOUD_FIRESTORE"
}

resource "google_artifact_registry_repository" "oauth2_code_flow_proxy" {
  provider      = google-beta
  location      = module.common.google_region
  repository_id = "oauth2-code-flow-proxy"
  description   = "Docker registry for oauth2 code flow containers"
  format        = "DOCKER"
}

resource "google_firebase_project" "default" {
  provider = google-beta.firebase
  project  = google_project.default.project_id
}

resource "google_firebase_web_app" "oauth2_code_flow_proxy" {
  provider     = google-beta.firebase
  project      = google_project.default.project_id
  display_name = "OAuth2 Code Flow proxy"

  depends_on = [
  google_firebase_project.default]
}

// TODO: Change buildkit to be default at docker level
resource "docker_image" "oauth2_code_flow_proxy_backend" {
  name = "oauth2_code_flow_proxy_backend"
  build {
    path       = ".."
    target     = "backend"
    dockerfile = "docker/oauth2_code_flow_proxy.dockerfile"
  }
}