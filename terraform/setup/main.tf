module "common" {
  source = "../common"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google",
      version = "3.74.0",
    }
  }
}

provider "google" {
  project = module.common.google_project
  region  = module.common.google_region
}

resource "google_storage_bucket" "tf_state" {
  name     = module.common.tf_state_bucket_name
  location = "EU"

  versioning {
    enabled = true
  }
}
