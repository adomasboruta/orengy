# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.84.0"
  constraints = "3.84.0"
  hashes = [
    "h1:bp14sKWDIWGlUGiM5o5A9iwYmpdWUMBgnmQ3P+BX5rk=",
    "zh:2914cb02edbec609edd38e4fb3c1ecac0f2ea7cf1063dfa708219d1efb055ae5",
    "zh:37f1fff3ea2bbb7ad42616590bd37dc0162935636983b5b8349621c277a9fa17",
    "zh:3dcd721a1f91723a5397840ca1172a6ebcbcf3cf98a5103a1c02af82962cb2e7",
    "zh:4ae06664f3ce11f654ca38021d91c620cce9ce70f793568f444585258c5b9406",
    "zh:53b3722e7fd9ef52dc9e575990dd2acb8d1eba8f19fd71956b93058514e3793f",
    "zh:5450628e4656a59c4debc033c117f0faf79f1067191644b7c6bc4496b50cf227",
    "zh:ad54eafb01a85b6f02b45e06065ebd61812a1ccd4ff81d35b6142112fe89961b",
    "zh:b799a2db8923a7cf4372d15e513a4d0a6000ac919c56cf2900a322bce5dca32d",
    "zh:d62c1737112a0967372996349738f69a6f6093ad82e9509f54ac4f8643a78883",
    "zh:d6b57d185a394dd82c51e405d2b59a5ca9e9f9a0c70d89a3ffb52c288ffeef0e",
    "zh:f5492abbea348d5770aadbf7bbf7ff3f8b60299dca600d401cfe35665e2f5458",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version = "3.84.0"
  hashes = [
    "h1:NLyGpJc3eDZ0g0VoqbtO+JNIiTLqlm1oydUVd6XAqkk=",
    "zh:05e194bcf38bebbe3bac77183b9031765123dd3ca0a6d835a5edaf73313e6924",
    "zh:100f00236fdfd6ea3b1af3682b4fa8d38ad9a34ec9343ef5882e0e30474329b8",
    "zh:2ded8a5fec4707d0b5f6d1047b8456174b008e3029e1b2f530e6e5981a583a17",
    "zh:4aed2340a9e5019c9134ed05685658fbf73fecf23616d8fd9abdb13a42eabaee",
    "zh:6b1dc3654b2b52f189fcc62c60d2a474c3d8eec5827b2a1e2830c245a4601c16",
    "zh:98500b60dd83c2511dc0c646e01fbf2ccb1e2bc0d19aa715a42f7504faa57795",
    "zh:b24442550240c501cbfb08f3a5e1280d118c8606dedf8e8d90bae4a79c719b12",
    "zh:c189c94456f0895d938618c3cfb3ff40658ea9e1df01a58e78532e88d3e455ce",
    "zh:dd2d269ea55a3e1f9de0f649da40fd586340f64db0f6df1063fd43d14c1c37a7",
    "zh:fa1e82c95eb17e4310971f24047b8d90246deee1d19a1b14ed35404946a1ad52",
    "zh:fd734982dddb22c60a84ff1fde744d933aa9cef08c8150b7d82c72b611675400",
  ]
}

provider "registry.terraform.io/kreuzwerker/docker" {
  version     = "2.15.0"
  constraints = "2.15.0"
  hashes = [
    "h1:NEBNtKNukqr6qk5vWu8Xx/nHBVNNdlty0hrFG76K7AE=",
    "zh:0241e5c7b66c14aa54e367dfe380fbde8388d3254cbe8a70717c12f71897e82b",
    "zh:0f162f0a01ffe9eec32e78dfc2a5b05a373230c41f041439efa3f4b68903fdcb",
    "zh:1c222c1425fbb0367154bcb8e4d87b19d6eae133fbb341f73102fa3b300f34bf",
    "zh:679206433e31d8fa69d95167af6d2cd762218e89811b735ee20bd8da19f97854",
    "zh:a16baab045bc7a709a9767816304cc5548aa2ee0b72c0eee49e3826e6a46a3fd",
    "zh:a29c4e304a6a7faf5b651a61a91a1aa2e837591cff049fbe1c747b6319e43956",
    "zh:bed7a69dbb9a024aecfac840ca2ac2f1527062f3d9c8c718e4e81d464b5ad329",
    "zh:c22aa10806de216b6aa0b36a2c1114a9fdaa5b47925aaad3333de3ce24cc52c9",
    "zh:d034295663d8a39035fd6fdf0488b72544b13f48acaee797af967343248676f8",
    "zh:d9001dfeac0db1799d8ab9d04be090522529baae0dba7f7e82b71f2168f05582",
    "zh:d9f3eb7ef8f256eb9148d72bd4a00e34f3be5570484598e26b603cbdc5eed923",
    "zh:ef573f1e0f000127fa9f569c8ee320f03ba1d302a6fbf9aac8300efa6fc6f4b2",
    "zh:ef7e246b4205202161b4119c10a1282f74243e029508184132731f9f6f6d9f4c",
  ]
}
