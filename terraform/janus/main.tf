locals {
  ssh_host = cloudflare_record.janus_local.hostname
  ssh_port = 22
  ssh_user = "janus"

  cloudflare_dns_zone_id = "a8a730c74251df084663f387c496d07a"

  ssserver_rust_config_path = "/home/janus/projects/janus/ssserver_rust_config.json"
  inadyn_config_path        = "/home/janus/projects/janus/inadyn.conf"
  ssserver_rust_port        = 38388

  headscale_config_folder_path = "/home/janus/projects/janus/headscale"
  pihole_config_folder_path = "/home/janus/projects/janus/pihole"
}

provider "docker" {
  host     = "ssh://${local.ssh_user}@${local.ssh_host}:${local.ssh_port}"
  ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]
}

provider "ssh" {}

provider "random" {}

provider "cloudflare" {
  api_token = var.cloudflare_dns_api_token
}

resource "cloudflare_record" "janus_local" {
  zone_id = local.cloudflare_dns_zone_id
  name    = "janus.local"
  value   = "192.168.1.48"
  type    = "A"
  ttl     = 60
}

resource "random_password" "ssserver_rust" {
  length = 16
}

data "docker_registry_image" "ssserver_rust" {
  name = "ghcr.io/shadowsocks/ssserver-rust:latest"
}

resource "docker_image" "ssserver_rust" {
  name          = data.docker_registry_image.ssserver_rust.name
  pull_triggers = [data.docker_registry_image.ssserver_rust.sha256_digest]
}

resource "docker_container" "ssserver_rust" {
  name    = "ssserver_rust"
  image   = docker_image.ssserver_rust.image_id
  restart = "always"
  ports {
    internal = local.ssserver_rust_port
    external = local.ssserver_rust_port
    protocol = "tcp"
  }
  ports {
    internal = local.ssserver_rust_port
    external = local.ssserver_rust_port
    protocol = "udp"
  }

  volumes {
    read_only      = true
    host_path      = local.ssserver_rust_config_path
    container_path = "/etc/shadowsocks-rust/config.json"
  }

  lifecycle {
    replace_triggered_by = [
      ssh_sensitive_resource.ssserver_rust_config
    ]
  }
}

resource "ssh_sensitive_resource" "ssserver_rust_config" {
  when = "create"

  host  = local.ssh_host
  port  = local.ssh_port
  user  = local.ssh_user
  agent = true

  pre_commands = [
    "mkdir -p /home/janus/projects/janus"
  ]

  file {
    content = jsonencode({
      server      = "0.0.0.0"
      server_port = local.ssserver_rust_port
      password    = random_password.ssserver_rust.result
      method      = "aes-256-gcm"
    })
    destination = local.ssserver_rust_config_path
    permissions = "0600"
  }
}

variable "cloudflare_dns_api_token" {
  type     = string
  nullable = false
}


resource "ssh_sensitive_resource" "inadyn_config" {
  when = "create"

  host  = local.ssh_host
  port  = local.ssh_port
  user  = local.ssh_user
  agent = true

  pre_commands = [
    "mkdir -p /home/janus/projects/janus"
  ]

  file {
    content     = <<-EOT
      provider cloudflare.com {
        username = ${local.cloudflare_dns_zone_id}
        password = ${var.cloudflare_dns_api_token}
        hostname = janus.orengy.tech
        ttl = 60
        proxied = false
      }
    EOT
    destination = local.inadyn_config_path
    permissions = "0600"
  }
}


data "docker_registry_image" "inadyn" {
  name = "troglobit/inadyn:latest"
}

resource "docker_image" "inadyn" {
  name          = data.docker_registry_image.inadyn.name
  pull_triggers = [data.docker_registry_image.inadyn.sha256_digest]
}

resource "docker_container" "inadyn" {
  name    = "inadyn"
  image   = docker_image.inadyn.image_id
  restart = "always"

  volumes {
    read_only      = true
    host_path      = local.inadyn_config_path
    container_path = "/etc/inadyn.conf"
  }

  lifecycle {
    replace_triggered_by = [
      ssh_sensitive_resource.inadyn_config
    ]
  }
}

# Router firewall

resource "ssh_sensitive_resource" "headscale_config" {
  when = "create"

  host  = local.ssh_host
  port  = local.ssh_port
  user  = local.ssh_user
  agent = true

  pre_commands = [
    "mkdir -p ${local.headscale_config_folder_path}"
  ]

  file {
    content     = templatefile("${path.module}/headscale_config.yaml", {})
    destination = "${local.headscale_config_folder_path}/config.yaml"
    permissions = "0600"
  }
}

data "docker_registry_image" "headscale" {
  name = "headscale/headscale:0.22.3-debug"
}

resource "docker_image" "headscale" {
  name          = data.docker_registry_image.headscale.name
  pull_triggers = [data.docker_registry_image.headscale.sha256_digest]
}

resource "docker_container" "headscale" {
  name    = "headscale"
  image   = docker_image.headscale.image_id
  restart = "always"
  command = ["headscale", "serve"]

  volumes {
    host_path      = local.headscale_config_folder_path
    container_path = "/etc/headscale/"
    read_only = false
  }

  ports {
    internal = 8080
    external = 8080
    protocol = "tcp"
  }
  ports {
    internal = 9090
    external = 9090
    protocol = "tcp"
  }
  ports {
    internal = 50443
    external = 50443
    protocol = "tcp"
  }
  ports {
    internal = 80
    external = 80
    protocol = "tcp"
  }

  lifecycle {
    replace_triggered_by = [
      ssh_sensitive_resource.headscale_config
    ]
  }
}
# PI Hole

#resource "ssh_sensitive_resource" "pihole_config" {
#  when = "create"
#
#  host  = local.ssh_host
#  port  = local.ssh_port
#  user  = local.ssh_user
#  agent = true
#
#  pre_commands = [
#    "mkdir -p ${local.pihole_config_folder_path}"
#  ]
#
##  file {
##    content     = templatefile("${path.module}/headscale_config.yaml", {})
##    destination = "${local.headscale_config_folder_path}/config.yaml"
##    permissions = "0600"
##  }
#}

data "docker_registry_image" "pihole" {
  name = "pihole/pihole:latest"
}

resource "docker_image" "pihole" {
  name          = data.docker_registry_image.pihole.name
  pull_triggers = [data.docker_registry_image.pihole.sha256_digest]
}

resource "docker_container" "pihole" {
  name    = "pihole"
  image   = docker_image.pihole.image_id
  restart = "unless-stopped"
#  command = ["headscale", "serve"]

  volumes {
    host_path      = "${local.pihole_config_folder_path}/etc-pihole"
    container_path = "/etc/pihole"
    read_only = false
  }
  volumes {
    host_path      = "${local.pihole_config_folder_path}/etc-dnsmasq.d"
    container_path = "/etc/dnsmasq.d"
    read_only = false
  }

  ports {
    internal = 80
    external = 8888
    protocol = "tcp"
  }
  ports {
    internal = 53
    external = 54
    protocol = "tcp"
  }
  ports {
    internal = 53
    external = 54
    protocol = "udp"
  }

  env = [
    "WEBPASSWORD=P!QyWZk%CVsG2Cia"
  ]

#  lifecycle {
#    replace_triggered_by = [
#      ssh_sensitive_resource.pihole_config
#    ]
#  }
}






### VPN

# docker run \
#--name headscale \
#--detach \
#--volume $(pwd)/config:/etc/headscale/ \
#--publish 127.0.0.1:8080:8080 \
#--publish 127.0.0.1:9090:9090 \
#headscale/headscale:<VERSION> \
#headscale serve